#!/usr/bin/perl -w

#	Script to run glimmer and parse glimmer output into EMBL formatted
#	features for reading into Artemis annotation environment.  The script
#	requires a .fasta file and produces a raw .fasta.glimmer.out file on disk.
#	EMBL formatted output is directed to STDOUT and can piped to an appropriate
#	output file, if not the EMBL format will be displayed on the screen.
#	LmjF_art_glimmer runs glimmer on L. major trained model
#       NOTES:
#	1:	The script will not EMBL-format shadowed genes.
#	2:	Minimum gene length for the script is 75 bp. Change $gene_length to 
#		set your own gene length.
#
#	Usage: art_glimmer <sequence.fasta> > [optional_EMBL_file.tab]
#
#	Paul McDonagh, SBRI, September 1999, updated by Dhileep Sivam, about 11 years later.

###################################################################################

#user-defined variables
my $target_seq = $ARGV[0];
my $gene_length =75;
my $training_set = $ARGV[1];

#internal variables
my $inter_output = 'glimmer_out';
my $final_output = $ARGV[2];

system("glimmer3 -l -f -g $gene_length $target_seq $training_set $inter_output");
parse_glimmer($inter_output . '.predict', $final_output);


###################################################################################
sub parse_glimmer
{
#subroutine variable declarations
my $glimmer;  		#filehandle for input
my $output;		#filehandle for output
my $buffer;		#input buffer
my $FLAG_parse=1;	#FLAG to say it's time to parse
my $FLAG_shadow=0;	#FLAG t say it's a shadowed gene
my @results;		#results array for the genes
my $component;		#component of the result, ie gene, start and stop
my $inx;		#index counter

#get these variables from subroutine arguments
$glimmer = $_[0];
$output  = $_[1];

open(GLIMMER, $glimmer) || die "cannot open the input $glimmer file";
open (OUT, ">" . $output) || die "Cannot open the output file";

#grab the header
my $header = <GLIMMER>;

	while(<GLIMMER>){	

	#if(/Shadowed/)
	#{
	#	$FLAG_shadow = 1;
	#}
	#else
	#{
	#	$FLAG_shadow = 0;
	#}
	
	if(($FLAG_parse == 1) && ($FLAG_shadow == 0))
	{
		@results = split;
		$gene = $results[0];
		$start = $results[1];
		$end = $results[2];

		if($end < $start)
		{
		    	print OUT "FT   CDS              complement($start..$end)\n";
			print OUT "FT                   \/note=\"predicted using Glimmer\"\n";
			print OUT "FT                   \/gene=\"\"\n";
		}
		else
		{
			print OUT "FT   CDS              $start..$end\n";
			print OUT "FT                   \/note=\"predicted using Glimmer\"\n";
			print OUT "FT                   \/gene=\"\"\n";
		}		
	}
	
	if(/Putative/)
	{	
		$FLAG_parse = 1;
	}
	
	}
}
