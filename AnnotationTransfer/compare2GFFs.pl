#!/depot/perl-5.12.1/bin/perl
#
# Purpose: To compare SBRI and TriTryp GFFs to obtain different subsets for
#          annotating GENEDB
#
# Input : GFF3
#
################################

use strict;
use warnings;
use Data::Dumper;
use Bio::SeqIO;
use Bio::DB::GFF;
use Bio::DB::Fasta;
use Array::IntSpan;
use Bio::Tools::GFF;
use Math::Round;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'sbrigff=s',
              'gff=s',
              'fasta=s',
              'feat_type=s',
			  'find_modified_atg',
			  'find_modified_stop',
			  'ignore_ids=s',
              'debug',
              'help|h'
              );

my $usage = qq{
$0 
	--gff
		GFF file
	--sbrigff
		GFF file from sbri
	--fasta
		Fasta of the genome contigs	
	--feat_type
		Feature type
	-r, --rawcount
		Row count file with 5 columns
		Output of 
		<CHR><Position><TotalReads><PosStrandReads><NegStrandReads><MajorStrand>
	-bpgff
		breakpoint gff
	-utrdata 
		UTR status file in tab format <Gene><5utr><3utr>	
	--count_type
		maximal|slsite
	--ignore_ids
		Ignore these features when reading GFF or constructing coordinates
	
};

print "$usage\n" if($options{help});
$options{minPercentDiff} ||= 10;
$options{count_type} = 'maximal';
#----------------------------------------------------------------------
# Main - Read input
#----------------------------------------------------------------------
# Read fasta
my $fasta_href = &hasify_fasta($options{fasta});

# Read ignore ids list
my %ignore = ();
if($options{ignore_ids}){
	open(READ, $options{ignore_ids});
	while(my $line = <READ>){
		chomp($line);
		$ignore{$line}=1;
	}
}

# Read GFF from TriTryp/sanger
my $gff_href = &hasify_gff_all($options{gff}, \%ignore);
my %gff = %$gff_href;

# Now read sbri gff to compare against
my $gffio = Bio::Tools::GFF->new( -file => $options{sbrigff},
                                  -gff_version => 3);

while(my $feature = $gffio->next_feature()){

        my $type = $feature->primary_tag;
		next if $type ne $options{feat_type};
		
        my ($fid) = $feature->each_tag_value("ID");
		next if !exists $gff{$fid};
		
		my $strand = $feature->strand;
		my $contigid = $feature->seq_id;
		
		
		my $atgpos = $feature->start;
		$atgpos = $feature->end if $strand eq -1;
		my $stoppos = $feature->end;
		$stoppos = $feature->start if $strand eq -1;
		
		if($options{find_modified_atg}){
			my $atg_change_tag = "None";
			if($atgpos != $gff{$fid}{atgpos}){
				$atg_change_tag = $gff{$fid}{atgpos}."->".$atgpos;
				$feature->add_tag_value('atgchange', $atg_change_tag);
				$feature->remove_tag('description') if $feature->has_tag('description'); 
				my $gffstr = $gffio->gff_string($feature);
				print $gffstr."\n";
			}
	 	}
	 	if($options{find_modified_stop}){
			my $stop_change_tag = "None";
			if($stoppos != $gff{$fid}{stoppos}){
				$stop_change_tag = $gff{$fid}{stoppos}."->".$stoppos;
				$feature->add_tag_value('stopchange', $stop_change_tag);
				$feature->remove_tag('description') if $feature->has_tag('description'); 
				my $gffstr = $gffio->gff_string($feature);
				print $gffstr."\n";
			}
	 	
	 	
	 	}				
}                                  



sub hasify_gff_all{
	my ($gff_file, $ignore) = @_;
	my $gffio = Bio::Tools::GFF->new( -file => $gff_file,
                                  -gff_version => 3);

    my %gff = ();
    while(my $feature = $gffio->next_feature()){

        my $type = $feature->primary_tag;
		next if $type ne $options{feat_type};
		
        my ($fid) = $feature->each_tag_value("ID");
        next if $$ignore{$fid};
		
		my $contigid = $feature->seq_id;
			
        $gff{$fid}{"contigid"}=$feature->seq_id;
        $gff{$fid}{"fsource"}=$feature->source_tag;
        $gff{$fid}{"ftype"}=$feature->primary_tag;
        $gff{$fid}{"fmin"}=$feature->start;
        $gff{$fid}{"fmax"}=$feature->end;
        $gff{$fid}{"fstrand"}=$feature->strand;
        $gff{$fid}{"fscore"}=$feature->score;
        $gff{$fid}{"fframe"}=$feature->frame;
        if($gff{$fid}{"fstrand"} eq 1){
        	$gff{$fid}{"atgpos"}=$feature->start;
        	$gff{$fid}{"stoppos"}=$feature->end;
        }elsif($gff{$fid}{"fstrand"} eq -1){
        	$gff{$fid}{"atgpos"}=$feature->end;
        	$gff{$fid}{"stoppos"}=$feature->start;
        }
        
        
        my %attr = ();
		foreach my $tag ( $feature->all_tags ) {
			my $str;
       		foreach my $value ( $feature->each_tag_value($tag) ) {
	    		$str .= "$value,";
      		}
      		$str =~ s/,$//;
      		$attr{$tag}=$str;
   		}
		$gff{$fid}{"attr"}=\%attr;

    }
	return \%gff;
}

sub hasify_gff_bystart(){
	my ($gff_file, $ignore) = @_;
	my $gffio = Bio::Tools::GFF->new( -file => $gff_file,
                                  -gff_version => 3);

    my %bystart = ();
    while(my $feature = $gffio->next_feature()){
    	my ($fid) = $feature->each_tag_value("ID");
    	next if $$ignore{$fid};
		my $contigid = $feature->seq_id;
		$bystart{$contigid}{$feature->start}{fid} = $fid;	
    }
	return \%bystart;
}


sub hasify_bpgff(){
	my ($gff_file) = @_;
	my $gffio = Bio::Tools::GFF->new( -file => $gff_file,
                                  -gff_version => 3);

    my %gff = ();
    while(my $feature = $gffio->next_feature()){

        my $type = $feature->primary_tag;
        my ($fid) = $feature->each_tag_value("ID");
        my ($hasZero) = $feature->each_tag_value("hasZero");
        my ($length) = $feature->each_tag_value("length");
		my $contigid = $feature->seq_id;
			
        $gff{$fid}{"contigid"}=$feature->seq_id;
        $gff{$fid}{"fsource"}=$feature->source_tag;
        $gff{$fid}{"ftype"}=$feature->primary_tag;
        $gff{$fid}{"fmin"}=$feature->start;
        $gff{$fid}{"fmax"}=$feature->end;
        $gff{$fid}{"fstrand"}=$feature->strand;
        $gff{$fid}{"fscore"}=$feature->score;
        $gff{$fid}{"fframe"}=$feature->frame;
        $gff{$fid}{"hasZero"}=$hasZero;
        $gff{$fid}{"length"}=$length;
    }
	return \%gff;
}

sub hasify_bpgff_bystart(){
	my ($gff_file) = @_;
	my $gffio = Bio::Tools::GFF->new( -file => $gff_file,
                                  -gff_version => 3);

    my %bystart = ();
    while(my $feature = $gffio->next_feature()){
    	my ($fid) = $feature->each_tag_value("ID");
		my $contigid = $feature->seq_id;
		$bystart{$contigid}{$feature->start}{fid} = $fid;	
    }
	return \%bystart;
}

sub hasify_fasta(){
    my $file = shift;
    # Read and hasify fasta
    my %fasta = ();
    my $fastaio = Bio::SeqIO->  new( -file => $file,
                                    -format => 'fasta' ) or die "Fail reading Fasta file : $file\n";
    while(my $seq = $fastaio->next_seq()){
        my $id = $seq->id;
        my $residues = $seq->seq();
        $residues =~ s/\*//;
        my $len = length($residues);
        $fasta{$id}{seq}=$residues;
        $fasta{$id}{len}=$len;

    }
    return (\%fasta);
}
