#!/depot/perl-5.12.1/bin/perl
#
# Purpose: To spit transposable element gff from geneious gff
#
# Input : GFF3
#
################################

use strict;
use warnings;
use Data::Dumper;
use Bio::SeqIO;
use Bio::DB::GFF;
use Bio::DB::Fasta;
use Array::IntSpan;
use Bio::Tools::GFF;
use Math::Round;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'geneiousgff=s',
              'debug',
              'help|h'
              );

my $usage = qq{
$0 
	--geneiousgff
		Geneious GFF file
	
};

print "$usage\n" if($options{help});

#----------------------------------------------------------------------
# Main - Read input
#----------------------------------------------------------------------

my $gffio = Bio::Tools::GFF->new( -file => $options{geneiousgff},
                                  -gff_version => 3);


while(my $feature = $gffio->next_feature()){
	
    my $type = $feature->primary_tag;
    if ($type eq 'Transposable element'){
    	$feature->primary_tag('transposable_element');
    	$feature->source_tag('MannualAnnotation');
    	$feature->remove_tag('created by') if $feature->has_tag('created by');
	    $feature->remove_tag('modified by') if $feature->has_tag('modified by');
	    my $feature_string = $gffio->gff_string($feature);
	    print $feature_string."\n";
    }
		
}
