#!/depot/perl-5.12.1/bin/perl
#
#  Split maker produced gff (contigxx.gff) into multiple gffs according to its source (column 2)
#  This helps spearte GFFs of individual abinitio predictions/est/protein blasts/
#  Later one could use makers' '' script convert match-matchpart into gene-mRNA-cds level gff3
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'prefix=s',
              'source|s=s',
              'format|f=s',
              'feat_type|ft=s',
              'set_feat_type_to=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    -g, --gff
        Maker produced GFF file
        Might work with any GFF with different sources
    -s, --source
        Name of source to make GFF for. Default: all
        eg, augustus
    -f, --format
        [asis|standard]
        asis=> outputs as is
        standard=>converts match/match_parts into genes
        For Crit. match_part seems to match genes. 
        Programs (genemark & augustus) over predicts multiexonic genes. So, better to treat match_parts as genes.
    -ft, --feat_type
        Type of the future to match and convert from
        eg (match or match_part)
        Works only with -format=standard
    --set_feat_type_to
        Set the future to convert to
        Default: gene
        Works only with -format=standard & -feat_type is set
    --prefix
        something to name a file
        Default: prefix 
    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
die("$usage\n") if(!$options{gff});
$options{prefix} ||= $options{gff};
$options{prefix} =~ s/\.gff.*//;
$options{format} ||= 'asis';
$options{set_feat_type_to} ||= 'gene';
#read in gff
if(-e $options{gff}){
    my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                 -gff_version => 3);

    my %feats = ();my $masterfeat;
    while(my $feature = $gffio->next_feature()){
        my $ftype = $feature->primary_tag;
        my $fsource = $feature->source_tag;
        my $fstart = $feature->start;
        my $fend = $feature->end;
        
        $masterfeat = $feature if $fsource eq '.'; # save if its contig feature
        my($mfstart, $mfend) = ($masterfeat->start, $masterfeat->end);
        
        #add to hash except for contig feature
        if($fstart ne $mfstart && $fend ne $mfend){
            push(@{$feats{$fsource}}, $feature) if $fsource ne '.';
        } 
        
    }                                  
    
    print Dumper(\%feats) if $options{debug};
    
    foreach my $source (keys %feats){  #iterate thru each fsource
    	next if($options{source} && $options{source} ne $source);
    	print "Printing $source\n" if $options{debug};
    	my $outfile = $options{prefix}."_".$source.".gff3";
        $outfile =~ s/\:/_/g;
        my $masterstring = $gffio->gff_string($masterfeat);

    	open(my $WRITE, ">>$outfile"); 
    	print $WRITE "##gff-version 3\n";  #add master feature & header
    	print $WRITE $masterstring."\n";
    	foreach my $feat (@{$feats{$source}}){
    		my $type = $feat->primary_tag;
    		if($options{format} eq 'standard'){
    			if($type eq $options{feat_type}){
    				$feat->primary_tag($options{set_feat_type_to});
    				my $gffstr = $gffio->gff_string($feat);
    				print $feat."\n" if $options{debug};
                    print $WRITE $gffstr."\n";
    			}
    		}else{
	    		my $gffstr = $gffio->gff_string($feat);
	    		print $feat."\n" if $options{debug};
	    		print $WRITE $gffstr."\n";
    		}
    	}
    	close($WRITE);
    }        


}else{
	die("Could not read $options{gff} file.\n");
}