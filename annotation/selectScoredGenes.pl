#!/depot/perl-5.12.1/bin/perl
#
#  Select genes from scores
#  
#  
#
##############
use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'scores=s',
              'min_score_4preds=s',
	      'min_score_4prot=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    --scores
        Score file with scores for each of the preds
        Fields are expected in following order.
        id, aug, am, gm, prot, total
    --min_score_4preds
	Minimum score from combined preds:Automagi, Augustus, Genemark

    --min_score_4prot
	Minimum score from prtein2genome, ususally 1
  
    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
$options{feat_type} ||= 'CDS';


open(READ, $options{scores});
while(my $line = <READ>){
	chomp($line);
	next if $line =~ /^#/;
	my $status = 'FAIL';
	my($id, $aug, $am, $gm, $prot, $total)=split("\t", $line);
	my $predtotal = $am + $aug + $gm;
	if($predtotal >= $options{min_score_4preds}){
	   $status = 'PASS';
	}else{
	   $status = 'PASS' if ($predtotal > 0 && $prot > 0);
	}
	
	print "$line\t$status\n";
	
	
}
