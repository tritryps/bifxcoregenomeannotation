#!/depot/perl-5.12.1/bin/perl
#
# Purpose: To compare manually edited geneious gffs and sanger gffs 
#
# Input : GFF3
#
################################

use strict;
use warnings;
use Data::Dumper;
use Bio::SeqIO;
use Bio::DB::GFF;
use Bio::DB::Fasta;
use Array::IntSpan;
use Bio::Tools::GFF;
use Math::Round;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'geneiousgff=s',
              'sangergff=s',
              'fasta=s',
              'feat_type=s',
              'ignore_ids=s',
              'debug',
              'help|h'
              );

my $usage = qq{
$0 
	--geneiousgff
		Geneious GFF file
	--sangergff
		GFF file from Sanger
	--fasta
		Fasta of the genome contigs	
	--feat_type
		Feature type
	--ignore_ids
		Ignore these features when reading GFF or constructing coordinates
	
};

print "$usage\n" if($options{help});

#----------------------------------------------------------------------
# Main - Read input
#----------------------------------------------------------------------
# Read fasta
my $fasta_href = &hasify_fasta($options{fasta});

# Read ignore ids list
my %ignore = ();
if($options{ignore_ids}){
	open(READ, $options{ignore_ids});
	while(my $line = <READ>){
		chomp($line);
		$ignore{$line}=1;
	}
}

my %feat_types =(gene=> 1, Pseudogene => 1); 
my $geneious_genes = &find_geneious_genes($options{geneiousgff}, \%feat_types);
print Dumper($geneious_genes) if $options{debug};

# genereate deleted/changed/new genes list
my %newgenes = ();
my %changedgenes = ();
my %fusedgenes = ();
my %keptgenes = ();

print "geneious genes name walkthru: begin\n" if $options{debug};
for my $gene (keys %$geneious_genes){
	print "\t$gene\n" if $options{debug};
	$keptgenes{$gene} = '1';
	$newgenes{$gene} = '1' if $$geneious_genes{$gene}{'name'} eq 'New gene';
	$changedgenes{$gene} = '1' if $$geneious_genes{$gene}{'name'} eq 'Changed gene';
	$fusedgenes{$gene} = '1' if $$geneious_genes{$gene}{'name'} eq 'Fused gene';
	
	#print "print only details for CFAC1_270037200";
	#print "$gene:\n";
	#print Dumper($$geneious_genes{$gene});
	#print Dumper($$geneious_genes{$gene}) if $gene eq 'CFAC1_270037200';
	#print Dumper($$geneious_genes{$gene}) if $gene eq 'CFAC1_270041900';
	#print "#################:\n";

}
print "geneious genes name walkthru: end\n" if $options{debug};

print "All Genes\n" if $options{debug};
print Dumper(\%keptgenes) if $options{debug};
print "New Genes\n" if $options{debug};
print Dumper(\%newgenes) if $options{debug};
print "Changed Genes\n" if $options{debug};
print Dumper(\%changedgenes) if $options{debug};
print "Fused Genes\n" if $options{debug};
print Dumper(\%fusedgenes) if $options{debug};


# Now walk thru Sanger GFF.
my %donttouch_feat_types = (gap => 1, rRNA=>1, snoRNA=>1, snRNA=>1, tRNA=>1, contig=>1);
my %donttouch_sources = ('Aragorn_1.2.36'=>1, INFERNAL=>1, GenomeTools=>1);
my %main_feat_types = (gene=>1, mRNA=>1, CDS=>1, exon=>1, polypeptide=>1);
my %tmm_feat_types = (membrane_structure=>1, cytoplasmic_polypeptide_region=>1, transmembrane_helix=>1, non_cytoplasmic_polypeptide_region=>1);
my %tmm_is_contained = ();
my %protein_match_types = (protein_match=>1);
my %portein_is_contained = ();

my $gffio = Bio::Tools::GFF->new( -file => $options{sangergff},
                                  -gff_version => 3);

my %codinggff = ();
my @noncodinggff = ();
while(my $feature = $gffio->next_feature()){
	my $sanger_feature_string = $gffio->gff_string($feature);
    my $sanger_type   = $feature->primary_tag;
    my $sanger_fmin   = $feature->start;	
    my $sanger_fmax   = $feature->end;	
    my $sanger_strand = $feature->strand;	
    my $sanger_source = $feature->source_tag;	

	my $sanger_chr = $feature->seq_id;
	my $fid = 'NA' ; my $name = 'NA';
	($fid) = $feature->each_tag_value("ID") if ($feature->has_tag('ID'));
	
	#if feature is one of dont touch type; just print its gff string and move to next feature
	push(@noncodinggff, $sanger_feature_string) if exists $donttouch_feat_types{$sanger_type};
	next if exists $donttouch_feat_types{$sanger_type};
	push(@noncodinggff, $sanger_feature_string) if exists $donttouch_sources{$sanger_source};
	next if exists $donttouch_sources{$sanger_source};
	
	#find geneid of the feature
	my $geneid = $fid;
	$geneid =~ s/\.1// if $sanger_type eq "mRNA";
	$geneid =~ s/\.1\:CDS\:\d// if $sanger_type eq "CDS";
	$geneid =~ s/\.1\:exon\:\d// if $sanger_type eq "exon";
	$geneid =~ s/\.1:pep// if $sanger_type eq "polypeptide";
	
	# ignore genes that are deleted in geneious
	if (exists $keptgenes{$geneid}){
		#if the sanger_type is one of main types (gene/mrna/cds/exon/polypeptide) then adjust the coordinates. else simple check if contained and print/delete
		if (exists $main_feat_types{$sanger_type}){
			if (exists $changedgenes{$geneid}){
				if($sanger_strand eq $$geneious_genes{$geneid}{'strand'}){
					$feature->start($$geneious_genes{$geneid}{'fmin'});
					$feature->end($$geneious_genes{$geneid}{'fmax'});
					$feature->source_tag('ManualAnnotation');
					$feature->add_tag_value('is_pseudo', 'YES') if $$geneious_genes{$geneid}{'feat_type'} eq 'Pseudogene';
					#$feature->add_tag_value('SeattleBioMed_sanger_fmin', $sanger_fmin);
					#$feature->add_tag_value('SeattleBioMed_sanger_fmax', $sanger_fmax);
					#$feature->add_tag_value('SeattleBioMed_change_type', 'Changed Gene');
					#$feature->add_tag_value('SeattleBioMed_feat_type', $$geneious_genes{$geneid}{'feat_type'});
				}else{
					warn "$geneid: Original and edited genes are on the oppsite strand. This is not logical.\n";
				}
			}elsif (exists $fusedgenes{$geneid}){
				if($sanger_strand eq $$geneious_genes{$geneid}{'strand'}){
					$feature->start($$geneious_genes{$geneid}{'fmin'});
					$feature->end($$geneious_genes{$geneid}{'fmax'});
					$feature->source_tag('ManualAnnotation');
					$feature->add_tag_value('is_pseudo', 'YES') if $$geneious_genes{$geneid}{'feat_type'} eq 'Pseudogene';
					#$feature->add_tag_value('SeattleBioMed_sanger_fmin', $sanger_fmin);
					#$feature->add_tag_value('SeattleBioMed_sanger_fmax', $sanger_fmax);
					#$feature->add_tag_value('SeattleBioMed_change_type', 'Fused Gene');
					#$feature->add_tag_value('SeattleBioMed_feattype', $$geneious_genes{$geneid}{'feat_type'});
				}else{
					warn "$geneid: Original and edited genes are on the oppsite strand. This is not logical.\n";
				}
		
			}
			my $feature_string = $gffio->gff_string($feature);
			$codinggff{$sanger_chr}{$geneid}{$sanger_type}=$feature_string if $feature->has_tag('ID');
		} #endof main feattypes
	}# end of keptgenes for main feattypes. For TMM featype check is done on case by case basis below
		# if sanger_type is one of TMMHMM feat types then check if contained.
		# if contained within gene, print it. else delete it and its sub features
		if(exists $tmm_feat_types{$sanger_type}){
			warn "inside tmm feattypes\n" if $options{debug};
			if($sanger_type eq "membrane_structure"){
				$geneid =~ s/\.1:pep\.membrane// if $sanger_type eq "membrane_structure";
				if (exists $keptgenes{$geneid}){
					warn "\t$geneid $sanger_type\n" if $options{debug};
					warn "gff string $codinggff{$sanger_chr}{$geneid}{'gene'}\n" if $options{debug};
					my @gene_gff_string = split("\t", $codinggff{$sanger_chr}{$geneid}{'gene'});
					my $gene_fmin = $gene_gff_string[3];
					my $gene_fmax = $gene_gff_string[4];
					warn "\t$gene_fmin,  $gene_fmax, $sanger_fmin, $sanger_fmax\n" if $options{debug};
					my $is_contained = is_subfeature_contained_within_feature( $gene_fmin,  $gene_fmax, $sanger_fmin, $sanger_fmax);
					if ($is_contained eq 'YES'){
						warn "\t\t YES\n" if $options{debug};
						$tmm_is_contained{$geneid}=1;
						my $feature_string = $gffio->gff_string($feature);
						if (exists $codinggff{$sanger_chr}{$geneid}{'multiple'}){
							$codinggff{$sanger_chr}{$geneid}{'multiple'} .= "\n".$feature_string if exists $keptgenes{$geneid};
						}else{
							$codinggff{$sanger_chr}{$geneid}{'multiple'} =  $feature_string if exists $keptgenes{$geneid};
						}
					}
				}
			}else{
				($geneid) = $feature->each_tag_value("Parent") if ($feature->has_tag('Parent'));
				$geneid =~ s/\.1:pep\.membrane//;
				warn "\t$geneid $sanger_type\n" if $options{debug};
				if (exists $tmm_is_contained{$geneid}){	
					warn "\t\t YES\n" if $options{debug};
					my $feature_string = $gffio->gff_string($feature);
					$codinggff{$sanger_chr}{$geneid}{'multiple'} .= "\n".$feature_string if exists $keptgenes{$geneid};
					
				}
			}
		}
		# if sanger_type is one of protein_match feat types then check if contained.
		# if contained within gene, print it. else delete it and its sub features
		if(exists $protein_match_types{$sanger_type}){
			warn "inside protein_match feattypes\n" if $options{debug};
			if($sanger_type eq "protein_match"){
				($geneid) = $feature->each_tag_value("Parent") if ($feature->has_tag('Parent'));
				$geneid =~ s/\.1:pep// if $sanger_type eq "protein_match";
				if (exists $keptgenes{$geneid}){
					warn "\t$geneid $sanger_type\n" if $options{debug};
					warn "gff string $codinggff{$sanger_chr}{$geneid}{'gene'}\n" if $options{debug};
					my @gene_gff_string = split("\t", $codinggff{$sanger_chr}{$geneid}{'gene'});
					my $gene_fmin = $gene_gff_string[3];
					my $gene_fmax = $gene_gff_string[4];
					print "\t$gene_fmin,  $gene_fmax, $sanger_fmin, $sanger_fmax\n" if $options{debug};
					my $is_contained = is_subfeature_contained_within_feature( $gene_fmin,  $gene_fmax, $sanger_fmin, $sanger_fmax);
					if ($is_contained eq 'YES'){
						warn "\t\t YES\n" if $options{debug};
						my $feature_string = $gffio->gff_string($feature);
						if (exists $codinggff{$sanger_chr}{$geneid}{'multiple'}){
							$codinggff{$sanger_chr}{$geneid}{'multiple'} .= "\n".$feature_string if exists $keptgenes{$geneid};
						}else{
							$codinggff{$sanger_chr}{$geneid}{'multiple'} =  $feature_string if exists $keptgenes{$geneid};
						}
					}
				}
			}
		}
		
	
}
print Dumper(\%codinggff) if $options{debug};


my @coding_feat_type_printorder = ('gene', 'mRNA', 'CDS', 'exon', 'polypeptide', 'multiple');
foreach my $contigid (sort keys %codinggff){
	foreach my $gene (sort keys %{$codinggff{$contigid}}){
		#print Dumper($codinggff{$contigid}{$gene});
		foreach my $type (@coding_feat_type_printorder){
			print $codinggff{$contigid}{$gene}{$type}."\n" if exists $codinggff{$contigid}{$gene}{$type};
		}
		
		print "#########\n";
	}
}


#print new genes
my @newgene_coding_feat_type_printorder = ('gene', 'mRNA', 'CDS', 'exon', 'polypeptide');
for my $geneid (keys %newgenes){
	my $id = ""; my $parentid = "";
	foreach my $type (@newgene_coding_feat_type_printorder){
		$id = $geneid if $type eq 'gene';
		$id = $geneid.'.1' if $type eq 'mRNA';
		$id = $geneid.'.1:CDS:1' if $type eq 'CDS';
		$id = $geneid.'.1:pep' if $type eq 'polypeptide';
		$id = $geneid.'.1:exon:1' if $type eq 'exon';
		
		$parentid = $geneid  if $type eq 'mRNA';
		$parentid = $geneid.'.1'  if $type eq 'CDS';
		$parentid = $geneid.'.1'  if $type eq 'exon';
		my $drivesfrom = $geneid.'.1';
		
		my $strand = '+';
		$strand = '-' if $$geneious_genes{$geneid}{'strand'} eq '-1';
		my $string = "$$geneious_genes{$geneid}{'contigid'}\tNewManualAnnotation\t$type\t$$geneious_genes{$geneid}{'fmin'}\t";
		$string .= "$$geneious_genes{$geneid}{'fmax'}\t.\t$strand\t.\tID=$id\;";
		$string .= "Parent=$parentid\;" if $type eq 'mRNA';
		$string .= "Parent=$parentid\;" if $type eq 'CDS';
		$string .= "Parent=$parentid\;" if $type eq 'exon';
		$string .= "Derives_from==$drivesfrom\;" if $type eq 'polypeptide';

		$string .= "is_pseudo=YES;" if $$geneious_genes{$geneid}{'feat_type'} eq 'Pseudogene';  
		print $string."\n";
	}
}


sub is_subfeature_contained_within_feature{
	my ($gene_fmin,  $gene_fmax, $subfeature_fmin, $subfeature_fmax) = @_;
	my $is_contained = 'NO';
	$is_contained = 'YES' if (($gene_fmin <= $subfeature_fmin) && ($gene_fmax >= $subfeature_fmax));
	return $is_contained;
}

sub find_geneious_genes{
	my ($gff_file, $feat_types) = @_;
	my $gffio = Bio::Tools::GFF->new( -file => $gff_file,
                                  -gff_version => 3);

    my %gff = ();
    while(my $feature = $gffio->next_feature()){
		my $feature_string = $feature->gff_string;
        my $type = $feature->primary_tag;
		next if !exists $$feat_types{$type};
		my $fid = 'NA' ; my $name = 'NA';
        if ($feature->has_tag('ID')){
        	($fid) = $feature->each_tag_value("ID");
        }else{
        	warn "missing ID:$feature_string\n";
        } 
        ($name) = $feature->each_tag_value("Name") if $feature->has_tag('Name'); 
		my $contigid = $feature->seq_id;
        $gff{$fid}{'name'}=$name;
        $gff{$fid}{'feat_type'}=$type;
        $gff{$fid}{'fmin'}=$feature->start;
        $gff{$fid}{'fmax'}=$feature->end;
        $gff{$fid}{'strand'}=$feature->strand;
        $gff{$fid}{'score'}=$feature->score;
        $gff{$fid}{'frame'}=$feature->frame;
        $gff{$fid}{'source'}=$feature->source_tag;
   		$gff{$fid}{'contigid'}=$contigid;
	}
   	print "end of geneious gff parsing\n" if $options{debug};
	return (\%gff);
}





sub hasify_fasta(){
    my $file = shift;
    # Read and hasify fasta
    my %fasta = ();
    my $fastaio = Bio::SeqIO->  new( -file => $file,
                                    -format => 'fasta' ) or die "Fail reading Fasta file : $file\n";
    while(my $seq = $fastaio->next_seq()){
        my $id = $seq->id;
        my $residues = $seq->seq();
        $residues =~ s/\*//;
        my $len = length($residues);
        $fasta{$id}{seq}=$residues;
        $fasta{$id}{len}=$len;

    }
    return (\%fasta);
}
