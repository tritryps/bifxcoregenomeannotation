#!/depot/perl-5.12.1/bin/perl
#
#  Define PTUs from one or more gff and a list of geneids (derived from orthology etc)
#  All the GFFs should be on same contig. Geneids should match ids in GFF.
#  
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'ids2consider|i=s',
              'feat_type=s',
              'contigid=s',
              'contigfasta=s',
              'contiglength=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    -g, --gff
        Comma separated list of gffs
	--feat_type
		Feature type to consider from GFF
		[CDS|gene|mRNA]
    -i, --ids2consider
		Id file, one per line
	-contigid
		Id of the contig/chr to prefix ptu numbers
		default CHR0
	--contigfasta
		Mutualy exclusive with --contiglen
		Fasta formated contig sequence
		Used to get length of contig
		Sequence is not used as such.
	--contiglength
		Lenght of the contig. Mutualy exclusive with --contigfasta
    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
die("$usage\n") if(!$options{gff});
die("Can NOT set both Contig Len and Contig Fasta options.\n\n $usage\n") if($options{contigfasta} && $options{contiglength});
die("WARNING: Missing contiglen/contigfasta. Setting it to be 10MB.n\n $usage\n") if(!$options{contigfasta} && !$options{contiglength});
#$options{contiglength} = 10000000 if (!$options{contigfasta} && !$options{contiglength});
$options{feat_type} ||= 'CDS';
$options{contigid} ||= 'CHR0';
#die("$usage\n") if(!$options{ids2consider});
# find lenght of contig if contig fasta is set
if($options{contigfasta}){
	my $seq_in = Bio::SeqIO->new(-file   => $options{contigfasta},
                             -format => "fasta" );
	while (my $inseq = $seq_in->next_seq) {
        my $seqid = $inseq->id;
        warn "WARNING: contigID supplied and in fasta file did not match\n" if $seqid ne $options{contigid};
        my $seq = $inseq->seq();
        $options{contiglength} = length($seq);
        }
}

## read in id file
my %ids = ();
open(READ, $options{ids2consider});
while(<READ>){
	chomp;
	$ids{$_}="1";
}

## read in gff
warn "reading GFF\n";

my $gff_href = &hasify_gff($options{gff}, $options{feat_type});
my %gff = %$gff_href;
print Dumper(\%gff)  if $options{debug};

#order geneids per locatio
my %bystart = ();
for my $geneid(sort keys %gff){
	my $fstart = $gff{$geneid}{fmin};
	print $fstart."\n"   if $options{debug};
	#$fstart .= ".0";
	for (1..10){
		next if !exists $bystart{$fstart};
		$fstart += "0.1" if exists $bystart{$fstart};
	}
	$bystart{$fstart}=$geneid;
}
print Dumper(\%bystart)   if $options{debug};

my @geneids_ordby_start;
foreach my $pos (sort {$a <=> $b} keys %bystart){
	push(@geneids_ordby_start, $bystart{$pos});
}
print @geneids_ordby_start   if $options{debug};

my %ptus = ();
my $ptu = "0";
my $ptuid = "0";
my ($ptustart, $ptuend, $ptustrand, $featcount) ;

foreach my $geneid (@geneids_ordby_start){
	chomp($geneid);
	my $seqid = $gff{$geneid}{fparent};
	my $source = $gff{$geneid}{fsource};
	my $start = $gff{$geneid}{fmin};
	my $end = $gff{$geneid}{fmax};
	my $ftype = $gff{$geneid}{ftype};
	my $score = $gff{$geneid}{fscore};
	my $strand = $gff{$geneid}{fstrand};
	my $phase = $gff{$geneid}{fphase};
	#my ($seqid, $source, $ftype, $start, $end, $score, $strand, $phase, $attrs) = split;	


  my $featid = $geneid;
  next if !exists $ids{$featid};  
  if($ptu eq "0"){
  	++$ptuid;
  	$ptustart = $start;
  	$ptuend = $end;
  	$ptustrand = $strand;
  	$featcount = "1";
  	
  	
  	$ptu = "1" if $ptustart;
  	$ptustart = "1";#reset to start first ptu from left end of chr.
  	next;
  }else{
  	if ($ptustrand eq $strand){
  		#in same PTU
  		$ptuend = $end;
  		++$featcount;
  	}elsif ($ptustrand ne $strand){
  		# in strand switch regions SO store current ptu and start new one
  		#store current
  		$ptus{$ptuid}{start}=$ptustart;
  		$ptus{$ptuid}{end}=$ptuend;
  		$ptus{$ptuid}{strand}=$ptustrand;
  		$ptus{$ptuid}{featcount}=$featcount;
  		
  		#start new
  		++$ptuid;
  		$ptustart = $start;
  		$ptuend = $end;
  		$ptustrand = $strand;
	 	$featcount = "1";
  	}
  
  }
  #don't forget last ptu
  $ptus{$ptuid}{start}=$ptustart;
  $ptus{$ptuid}{end}=$options{contiglength}; #sets last ptu's right boundary to chr end.
  $ptus{$ptuid}{strand}=$ptustrand;
  $ptus{$ptuid}{featcount}=$featcount;
}
print Dumper(\%ptus)   if $options{debug};

foreach my $ptuid (sort {$a<=>$b} keys %ptus){
	my $ptuname = "ptu_".$ptuid;
	print "$options{contigid}\t$ptuname\t$ptus{$ptuid}{strand}\t$ptus{$ptuid}{start}\t$ptus{$ptuid}{end}\t$ptus{$ptuid}{featcount}\n";
}
warn "OK\n";
 
 
 
 
 
#----------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------
 
 
sub hasify_gff(){
    my ($gff_files, $type) = shift;
	my %gff = ();                             

    my @files = split(',', $gff_files);
    foreach my $file (@files){
		if(-e $file){
		my $gffio = Bio::Tools::GFF->new( -file => $file,
										  -gff_version => 3);
									  
		my $geneId = "";
		while(my $feature = $gffio->next_feature()){
		
			my $ftype = $feature->primary_tag;
			if($ftype eq $options{feat_type}){     
					my @fid = $feature->each_tag_value("ID");
					my $geneid = $fid[0];
					#my @fcolor = $feature->each_tag_value("colour");
					#my $fcolor =  $fcolor[0];
					$gff{$geneid}{"fparent"}=$feature->seq_id;
					$gff{$geneid}{"fmin"}=$feature->start;
					$gff{$geneid}{"fmax"}=$feature->end;
					$gff{$geneid}{"fstrand"}=$feature->strand;
					$gff{$geneid}{"ftype"}=$feature->primary_tag;
					$gff{$geneid}{"fframe"}=$feature->frame;
					$gff{$geneid}{"fsource"}=$feature->source_tag;
					$gff{$geneid}{"fscore"}=$feature->score;
				}
			
			}
		}
	} 
	print Dumper(\%gff)  if $options{debug};
        return \%gff;
}
