#!/depot/perl-5.12.1/bin/perl
#
# When length and readCount tags are present, it calculates read density (per kb) and adds it as a tag.
# 
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Math::Round;
use Data::Dumper;
use URI::Escape;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'overwrite',
              'prefix=s',
              'feat_type|f=s',
              'print_all_features',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    -g, --gff
        Any GFF file
        Might work with any GFF with different sources
    -f, --feat_type
        Type of the future to add the tags to
    -overwrite
        If this is set, any new tags added will replace the existing content of the SAME tag
        Tags that are NOT added will not be affected.

    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
die("$usage\n") if(!$options{gff});
$options{prefix} ||= $options{gff};
$options{prefix} =~ s/\.gff.*//;
$options{feat_type} ||= 'gene';

#create outfile names and write handles
my $gff_base = $options{gff};
$gff_base =~ s/\.gff$//;


#print header
my $header = q{##gff-version	3
##feature-ontology	so.obo
##attribute-ontology	gff3_attributes.obo
};
print $header;

#read in gff
if(-e $options{gff}){
    my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                 		-gff_version => 3);

    while(my $feat = $gffio->next_feature()){
        my ($fid) = $feat->get_tag_values("ID");
        my $ori_ftype = $feat->primary_tag();
        my $gffstr = $gffio->gff_string($feat);
        if($ori_ftype eq $options{feat_type}){
        	my $readDensityPerKB = 0;
        	#get all the tag values for this gene...
        	my ($length) = $feat->each_tag_value("length") if $feat->has_tag("length");
        	my ($readCount) = $feat->each_tag_value("readCount") if $feat->has_tag("readCount");
        	my $tag = "readCount";
        	$readCount =~ s/\"//g;
        	$feat->remove_tag($tag) if ($feat->has_tag($tag) && $options{overwrite});
         	$feat->add_tag_value($tag, $readCount);
         	
        	next if (!$feat->has_tag("readCount") || !$feat->has_tag("length"));
		#next if $length == 0;
       		if ($length == 0){
			$readDensityPerKB = 0;
       		}else{
			$readDensityPerKB = round(($readCount / $length ) * 1000);
       		}
		my $tag = "readDensityPerKB";
       		$feat->remove_tag($tag) if ($feat->has_tag($tag) && $options{overwrite});
         	$feat->add_tag_value($tag, $readDensityPerKB);
         	my $gffstr = $gffio->gff_string($feat);
         	print  $gffstr."\n";
        }else{
            my $gffstr = $gffio->gff_string($feat);
            print  $gffstr."\n";
        }
        
    }                                  
}

