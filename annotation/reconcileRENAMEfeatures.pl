#!/depot/perl-5.12.1/bin/perl
#
# Purpose: To rename features per renamed chromosomes
#
# Input : GFF3
#
################################

use strict;
use warnings;
use Data::Dumper;
use Bio::SeqIO;
use Bio::DB::GFF;
use Bio::DB::Fasta;
use Array::IntSpan;
use Bio::Tools::GFF;
use Math::Round;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff=s',
              'renametable=s',
              'debug',
              'help|h'
              );

my $usage = qq{
$0 
	--gff
		GFF file to be renamed
	--renametable
		tab delmited file with two columns. First column contains original chrname, second indicates the new chr id
	
};

print "$usage\n" if($options{help});

#----------------------------------------------------------------------
# Main - Read input
#----------------------------------------------------------------------

#read in rename table
my %rename=();
open(READ, $options{renametable});
while(my $line=<READ>){
	chomp($line);
	my($fromname, $toname) = split("\t", $line);
	$rename{$fromname}=$toname;
}

my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                  -gff_version => 3);


while(my $feature = $gffio->next_feature()){
   	my $chr = $feature->seq_id;
   	my $feature_string = $gffio->gff_string($feature);
	if(exists $rename{$chr}){
    	my $to = $rename{$chr};
    	my $CHR = uc $chr;
    	my $TO = uc $to;
    	
    	
    	$feature_string =~ s/$chr/$to/g;
    	$feature_string =~ s/$CHR/$TO/g;
    	
    }
   	print $feature_string."\n";	
}