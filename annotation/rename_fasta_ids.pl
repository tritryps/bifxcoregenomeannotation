#!/depot/perl-5.12.1/bin/perl
#
# Purpose: Rename ids in fasta using a mapfile
#
#
#
################################

use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Data::Dumper;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my $usage = qq{
    $0
    -f, --fasta
       fasta file name
    -m --mapfile
       map file name old ids in column1 and new ids incolumn2

    --debug
       debug mode
    -h, --help
       this help message
};

my %options =();
my $opt_obj = GetOptions (\%options,
              'fasta|f=s',
              'mapfile|m=s',
              'debug',
              'help|h',
            );

die("$usage\n") if($options{help});
die("$usage\n") if(!$options{fasta});
die("$usage\n") if(!$options{mapfile});

# read in map file
my %map=();
open(READ, $options{mapfile});
while(my $line =<READ>){
	chomp($line);
	my($old, $new) = split("\t", $line);
	$map{$old}=$new;
}
close(READ);
print Dumper(\%map) if $options{debug};

#read in gff
if(-e $options{fasta}){
	my $seq_obj1 = Bio::SeqIO->new( -format => 'fasta',
                                -file   => $options{fasta});

	while(my $seq = $seq_obj1->next_seq()){
	    my $id= $seq->id;
	    my $newid = $map{$id};
	    $newid = $id if !$newid;
	    my $seq = $seq->seq();
	    print ">$newid\n$seq\n";		
}	
}
