#!/depot/perl-5.12.1/bin/perl
#
# Purpose: To spit transposable element gff from geneious gff
#
# Input : GFF3
#
################################

use strict;
use warnings;
use Data::Dumper;
use Bio::SeqIO;
use Bio::DB::GFF;
use Bio::DB::Fasta;
use Array::IntSpan;
use Bio::Tools::GFF;
use Math::Round;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'sangergff=s',
              'debug',
              'help|h'
              );

my $usage = qq{
$0 
	--sangergff
		sanger GFF file
	
};

print "$usage\n" if($options{help});

#----------------------------------------------------------------------
# Main - Read input
#----------------------------------------------------------------------

my %donttouch_feat_types = (gap => 1, rRNA=>1, snoRNA=>1, snRNA=>1, tRNA=>1, contig=>1);
my %donttouch_sources = ('Aragorn_1.2.36'=>1, INFERNAL=>1, GenomeTools=>1);


my $gffio = Bio::Tools::GFF->new( -file => $options{sangergff},
                                  -gff_version => 3);


while(my $feature = $gffio->next_feature()){
	
    my $type = $feature->primary_tag;
    my $source = $feature->source_tag;
    if (exists $donttouch_sources{$source}){
    	my $feature_string = $gffio->gff_string($feature);
	    print $feature_string."\n";	
    }
    
 
		
}
