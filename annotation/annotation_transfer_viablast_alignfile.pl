#!/depot/perl-5.12.1/bin/perl

use strict;
use warnings;
use Bio::SeqIO;
use Data::Dumper;
use Bio::SearchIO;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
umask(0000);

#-------------------------------------------------------------------------------
# handle command line.
#-------------------------------------------------------------------------------

my %options =();
my $opt_obj = GetOptions (\%options,
              'blastfile=s',
              'prefix|b=s',
              'source=s',
              'min_cons|c=s',
              'min_cov|x=s',
              'min_iden=s',
              'score=s',
              'evalue=s',
              'target=s',
              'summary|s=s',
              'debug|d',
              'help|h');



my $usage = qq{
$0 
    -blastfile,
        Blastfile in align format
    --prefix
        something to name a file
        Default: prefix 
    -min_cons
        Minimum conservation
        Default:0
    -min_cov
        Minimum coverage (length of query in hsp/length of query)*100
        Default:0
    -min_iden
        Minimum identity
        Default:0
    -score
        Minimum score
        Default:100
    -evalue
        Minimum evalue
        Default:10e-6
    -target
        Name of the target blast database [TirTryp|NR]
        Default [TriTryp]
        THis is used mainly to parse the description field
    -h, --help
    -d, --debug
};

die("$usage\n") if($options{help});
die("$usage\n") if(!$options{blastfile});
$options{prefix} ||= "prefix";
$options{min_cons} ||= "0";
$options{min_cov} ||= "0";
$options{min_iden} ||= "0";
$options{score} ||= "100";
$options{evalue} ||= "e10-6";
$options{target} ||= 'TriTryp';



#parse blastoutput
my $in = new Bio::SearchIO( -format => 'blast',
                            -file   =>  $options{blastfile});
    
while ( my $result = $in->next_result ){
    my ($queryName, $queryLen) = ($result->query_name, $result->query_length);
    #   traverse thru each hit 
    my $hitCount = "0";
    my $isFirstHit = 'YES';
    while(my $hit = $result->next_hit){
        # parse hit name. The local pdb file has it in the second field of header 
        my ($hitName, $hitLen, $noofHsps, $hitDes)  = ($hit->name, $hit->length, $hit->num_hsps, $hit->description);
        my %hash =();
        $hitName =~ s/\|$//;
        if($options{target} eq 'TriTryp'){
	        foreach my $pair (split('\|', $hitDes)){
	        	next if length($pair) < 2;
	        	chomp($pair);
	            my($key, $value) =split("=", $pair);
	            $hash{$key}=$value;
	        }
        }elsif($options{target} eq 'NR'){
        	#print $hitDes;
        	my ($first, @rest) = split('\]', $hitDes);
        	my($name, $org) = split('\[', $first);
        	print $name."\t".$org."\n" if $options{debug};
        	$org ||= 'NA';
            $hash{product}='"'.$name.'"';
            $hash{organism}='"'.$org.'"';
        }
        print Dumper(\%hash) if $options{debug};
        #next;
        my $hspCount = "0";
        my $isFirstHsp = 'YES';
        #traverse each hsp
        while(my $hsp = $hit->next_hsp){
            ++$hspCount;
            my ($eValue, $fracIden, $fracCons, $gaps, $score) =  ($hsp->evalue, $hsp->frac_identical, $hsp->frac_conserved, $hsp->gaps, $hsp->score);                 
            my $cons = sprintf("%d", $fracCons * 100);
            # coverage = (len of query in_HSP/total len of query) *100
            my $covr = sprintf( "%d", 100 * $hsp->length('query') / $result->query_length );
            my $queryLen_inhsp = $hsp->length('query');
            my ($substart, $subend) = sort {$a <=> $b} ($hsp->start('hit'), $hsp->end('hit'));
            my $hspstrand = $hsp->strand('hit');
            my $substrand;
            $substrand = '-' if $hspstrand eq '-1';
            $substrand = '+' if $hspstrand eq '1';
            my $iden = sprintf ("%d", 100 * $fracIden);
            if($isFirstHit eq 'YES' && $isFirstHsp eq 'YES'){
                if($covr >= $options{min_cov} && $cons >= $options{min_cons} && $eValue <= $options{evalue} && $score >= $options{score}){
                    print "$queryName\t$hitName";
                    foreach my $key (sort keys %hash){
                    	print "\t$hash{$key}";
                    }
                    print "\t$covr\t$cons\t$eValue\t$score" if $options{debug};
                    print "\n";
                }
            }
               
            $isFirstHsp = "NO";
        }#while(my $hsp = $hit>next_hsp)
        
        ++$hitCount;
        $isFirstHit = 'NO'
    }#while(my $hit = $result->next_hit)
        
}#while ( my $result = $in->next_result )


















sub getFirstHitId_fromBlast(){
    my $oFile = shift;
    my $in = new Bio::SearchIO( -format => 'blast',
                                 -file   =>  $oFile);
    
    
    while ( my $result = $in->next_result ){
    	my $fHitName; my $fHitDes; my $seen_anyhit = "NO";
        my ($queryName, $queryLen, $query_description) = ($result->query_name, $result->query_length, $result->query_description);
        my $isFirstHit = "YES";  my $hitCount = "0";
        while(my $hit = $result->next_hit){
            my $hitName = $hit->accession;
            my $hitDes = $hit->description;
            $fHitName = $hitName if($isFirstHit eq "YES");
            $fHitDes = $hitDes if($isFirstHit eq "YES");
            $isFirstHit ="NO";++$hitCount;
            $seen_anyhit = 'YES';
            
            
        }
        $fHitName= 'NO_HITS_FOUND' if($seen_anyhit eq 'NO');
        $fHitDes= 'NO_HITS_FOUND' if($seen_anyhit eq 'NO');
        print "$queryName\t$fHitName\t$fHitDes\n";
    }
    
    #return $fHitName;            
}


sub blast2hash_ws(){
    my $oFile = shift;
    my %hResult =();
    
    #parse blastoutput
     my $in = new Bio::SearchIO( -format => 'blast',
                                 -file   =>  $oFile);
    
    while ( my $result = $in->next_result ){
        my ($queryName, $queryLen) = ($result->query_name, $result->query_length);
            
        #   traverse thru each hit 
        my $isFirstHit = "YES";my $hitCount = "0";
        while(my $hit = $result->next_hit){
            my ($hitName, $hitLen, $noofHsps)  = ($hit->name, $hit->length, $hit->num_hsps);
            my ($fHitName, $fHitLen) = ($hitName, $hitLen) if($isFirstHit eq "YES");
            my $hspCount = "0";
            
            #traverse each hsp
            my $isFirstHsp = "YES";
            while(my $hsp = $hit->next_hsp){
               ++$hspCount;
               my ($eValue, $fracIden, $fracCons, $gaps) =  ($hsp->evalue, $hsp->frac_identical, $hsp->frac_conserved, $hsp->gaps);                 
               # coverage = (len of query in_HSP/total len of query) *100
               my $covr = sprintf( "%d", 100 * $hsp->length('query') / $result->query_length );
               my ($fHitEvalue, $fHitFracIden, $fHitFracCons, $fHitCovr, $fHitGaps) =   ($eValue, $fracIden, $fracCons, $covr, $gaps) if($isFirstHit eq "YES");
               
               #build hash
               $hResult{$hitName}{$hspCount}{"queryName"}= $queryName;
               $hResult{$hitName}{$hspCount}{"queryLen"}= $queryLen;
               $hResult{$hitName}{$hspCount}{"blastOutFile"}=$oFile;
               $hResult{$hitName}{$hspCount}{"isFirstHsp"}=$isFirstHsp;
               $hResult{$hitName}{$hspCount}{"isFromFirstHit"}=$isFirstHit;
               $hResult{$hitName}{$hspCount}{"hitCount"}=$hitCount;
               $hResult{$hitName}{$hspCount}{"eValue"}=$eValue;
               $hResult{$hitName}{$hspCount}{"iden"}=sprintf( "%d", ($fracIden * 100));
               $hResult{$hitName}{$hspCount}{"cons"}=sprintf( "%d", ($fracCons * 100));
               $hResult{$hitName}{$hspCount}{"covr"}=$covr;
               
                                              
               $isFirstHsp = "NO";
            }#while(my $hsp = $hit>next_hsp)
        
        $isFirstHit ="NO";++$hitCount;
        }#while(my $hit = $result->next_hit)
        
    }#while ( my $result = $in->next_result )
    return \%hResult;            
}

