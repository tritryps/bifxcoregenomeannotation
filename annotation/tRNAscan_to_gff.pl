#!/depot/perl-5.12.1/bin/perl

use strict;
use warnings;
use Data::Dumper;
use File::Basename;
use Date::Simple ('date', 'today');
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
umask(0000);



#-------------------------------------------------------------------------------
# handle command line.
#-------------------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'infile=s',
              'summary|s=s',
              'debug|d=s',
              'help|h');
              
     
open(READ, $options{infile});
while(my $line = <READ>){
	next if $line =~ /^#/;
	chomp($line);
	my($contig, $tRNAcount, $tRNAbegin, $tRNAend, $tRNAtype, $tRNAanticodon, $intronb, $introne, $covescore) = split("\t", $line);
	$contig =~ s/\s//g;
	my $id = $contig."_tRNA".$tRNAcount."_".$tRNAtype;
	my $tRNAstrand = "+";
	$tRNAstrand = "-" if $tRNAend < $tRNAbegin;
	($tRNAbegin, $tRNAend) = sort {$a <=> $b} ($tRNAbegin, $tRNAend);
	print "$contig\ttRNAscan\tgene\t$tRNAbegin\t$tRNAend\t.\t$tRNAstrand\t.\tID=$id;anticodon=$tRNAanticodon;type=$tRNAtype;covescore=$covescore\n";

}