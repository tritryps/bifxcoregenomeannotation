#!/depot/perl-5.12.1/bin/perl
#
#  Add "Hypothetical protein" to all the genes with unknown product names
#
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Data::Dumper;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0
    -g, --gff
        gff
        
    --prefix
		something to name a file
		Default: prefix
	
		
    -h, --help
    -d, --debug
};

print $usage if $options{help};

my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
									  -gff_version => 3);

while(my $feature = $gffio->next_feature()){
	my $gffstr = $gffio->gff_string($feature);
	my $ftype = $feature->primary_tag;
	my $has_product = "NO";
	$has_product = "YES" if $feature->has_tag('product');
	my $product_tag =  '"hypothetical protein"';
	if($ftype eq 'gene' && $has_product eq 'NO'){
		$feature->add_tag_value('product', $product_tag);
		$feature->add_tag_value('product_source', '"product assigned denovo"');
		
	}
	if($ftype eq 'mRNA' && $has_product eq 'NO'){
		$feature->add_tag_value('product', $product_tag);
		$feature->add_tag_value('product_source', '"product assigned denovo"');
			}
	
	my $newgffstr = $gffio->gff_string($feature);
	
	print "$newgffstr\n";
	
}

#----------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------
