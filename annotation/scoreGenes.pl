#!/depot/perl-5.12.1/bin/perl
#
#  compares a mastergff with many other GFFs to see 
#  if features in master gff has overlapping feature in other gffs
#  
#
##############
use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'mastergff|g=s',
              'predgffs|p=s',
              'predorder|o=s',
              'feat_type|t=s',
              'prefix=s',
              'contigfasta=s',
              'contiglength=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    --mastergff, -g
        Master GFF file which contains the genes to be scored.
    --predgffs, -p
        Gffs for each prediction algorithms
    --predorder, -o
        Name of algorithms used in same order as gff inputs for predictions options
        values[AUGUSTUS,AUTOMAGI,GENEMARK] 
    --feat_type, -t
        Type of the feature[gene|mRNA|CDS]
        Default: CDS
    --prefix
        something to name a file
        Default: prefix 
    --contigfasta [not being used currently]
        Mutualy exclusive with --contiglen
        Fasta formated contig sequence
        Used to get length of contig
        Sequence is not used as such.
    --contiglength [not being used currently]
        Lenght of the contig. Mutualy exclusive with --contigfasta
    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
die("Predorder options is must.\n\n$usage\n") if(!$options{predorder});
die("Predgffs options is must.\n\n$usage\n") if(!$options{predgffs});

$options{feat_type} ||= 'CDS';
#die("Can NOT set both Contig Len and Contig Fasta options.\n\n $usage\n") if($options{contigfasta} && $options{contiglength});
#die("WARNING: Missing contiglen/contigfasta. Setting it to be 10MB.n\n $usage\n") if(!$options{contigfasta} && !$options{contiglength});

## read in gff
warn "reading GFF\n";

my $gff_href = &hasify_gff($options{mastergff}, $options{feat_type});
my %gff = %$gff_href;
print Dumper(\%gff)  if $options{debug};


#order geneids per location
=head
my %bystart = ();
for my $geneid(sort keys %gff){
    my $fstart = $gff{$geneid}{fmin};
    print $fstart."\n"   if $options{debug};
    #$fstart .= ".0";
    for (1..10){
        next if !exists $bystart{$fstart};
        $fstart += "0.1" if exists $bystart{$fstart};
    }
    $bystart{$fstart}=$geneid;
}
print Dumper(\%bystart)   if $options{debug};
=cut

#create a hash of alog-gffs
my %predgff = ();
my @predgff_array = split(',', $options{predgffs});
my $x = "0";
foreach my $algo (split(',', $options{predorder})){
    $predgff{$algo} = $predgff_array[$x];
    ++$x;
}
print Dumper(\%predgff) if $options{debug};



my %results = ();

foreach my $algo(keys %predgff){
	chomp($algo);
	print "$algo\t $predgff{$algo}\n" if $options{debug};
	sleep 5;
	my $gffrange = &rangify_gff($algo, $predgff{$algo}, $options{feat_type});
	
	my $am_predcount_href = &count_automagi_preds($predgff{$algo}) if($algo eq 'AUTOMAGI');
	#my %am_predcount = %$am_predcount_href;
	print  Dumper($am_predcount_href) if $options{debug};
	
	foreach my $geneid (sort keys %gff ){
		print $geneid."\t" if $options{debug};
		#sleep 1;
		my %hits =();
		foreach my $pos($gff{$geneid}{fmin}..$gff{$geneid}{fmax}){
			#print "$pos\t";
			my $predgeneid=$gffrange->lookup($pos);
			$hits{$predgeneid}="1" if $predgeneid;
			
		}
		my $ovlcount = keys %hits;
		my $genecount = "0";
		$genecount = 1 if $ovlcount > "0";
		my $genes = join(';', keys %hits);
		print "hits\t$genecount\t$genes\n" if $options{debug};
		# if prediction is from automagi, 
		# use the count of how many of 3 algorithms predicted it.
		if ($algo eq 'AUTOMAGI' && $genecount > "0"){
			my @am_predcounts = (); # to hold counts of algorithms for each of overlapping genes
			# cycle thru each of overlapping automagi gene
			foreach my $predid (keys %hits){
				my $count = $$am_predcount_href{$predid};
				push(@am_predcounts, $count);
			}
			@am_predcounts = sort @am_predcounts;
			$genecount = $am_predcounts[-1];
		}
        $results{$geneid}{$algo}=$genecount;
	    
	}
}

print Dumper(\%results) if $options{debug};

#print results
#print headers
print "GeneId\t";
foreach my $algo (sort keys %predgff){
    print $algo."\t";
}
print "TotalCount\n";

foreach my $geneid (sort keys %results){
	my $tot_count = "0";
	print "$geneid\t";
	foreach my $algo (sort keys %predgff){
		$tot_count += $results{$geneid}{$algo};
		print $results{$geneid}{$algo}."\t";
	}
	print "$tot_count\n";
}
#----------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------
 
 
sub hasify_gff(){
    my ($gff_files, $type) = shift;
    my %gff = ();                             

    my @files = split(',', $gff_files);
    foreach my $file (@files){
        if(-e $file){
        my $gffio = Bio::Tools::GFF->new( -file => $file,
                                          -gff_version => 3);
                                      
        my $geneId = "";
        while(my $feature = $gffio->next_feature()){
        
            my $ftype = $feature->primary_tag;
            if($ftype eq $options{feat_type}){
            	    my $attr_href = &process_attrs2($feature);
            	    my %attr = %$attr_href;    
                    my @fid = $feature->each_tag_value("ID");
                    my $geneid = $fid[0];
                    $gff{$geneid}{"fparent"}=$feature->seq_id;
                    $gff{$geneid}{"fmin"}=$feature->start;
                    $gff{$geneid}{"fmax"}=$feature->end;
                    $gff{$geneid}{"fstrand"}=$feature->strand;
                    $gff{$geneid}{"ftype"}=$feature->primary_tag;
                    $gff{$geneid}{"fframe"}=$feature->frame;
                    $gff{$geneid}{"fsource"}=$feature->source_tag;
                    $gff{$geneid}{"fscore"}=$feature->score;
                    $gff{$geneid}{"fattrs"}=$attr_href;
                }
            
            }
        }
    } 
    print Dumper(\%gff)  if $options{debug};
        return \%gff;
}

sub rangify_gff(){
    my ($algo, $gff_file, $type) = @_;
    my %gff = ();                             
    my $gffrange = Array::IntSpan->new();
    
    #my @files = split(',', $gff_files);
    if(-e $gff_file){
        my $gffio = Bio::Tools::GFF->new( -file => $gff_file,
                                          -gff_version => 3);
                                      
        my $geneId = "";
        while(my $feature = $gffio->next_feature()){
        
            my $ftype = $feature->primary_tag;
            if($ftype eq $options{feat_type}){
                    my $attr_href = &process_attrs2($feature);
                    my %attr = %$attr_href;    
                    my @fid = $feature->each_tag_value("ID");
                    my $geneid = $fid[0];
                    
                    $gffrange->set_range($feature->start, $feature->end, $geneid);
                }
            
            }
        }
     
    print $gffrange  if $options{debug};
    return $gffrange;
}

sub process_attrs2{
    my $feat = shift;
    
    my @tags = $feat->get_all_tags();
    
    my $attr;
    my %attr;
    foreach (@tags){
        my $tag = $_;
        my @values = $feat->get_tag_values($tag);
        foreach (@values){
            my $value = $_;
            my $quote_it;
            
            $quote_it++ if $value =~ /\s+/;
            $quote_it++ if $value =~ /;/;
            
            $value = '"'. $value . '"' if $quote_it;
            $attr{$tag} = $value;
        }
        
    }
    
  return \%attr;
}

sub count_automagi_preds(){
	my $amgff = shift;
	my %am_predcount = ();
	
	if(-e $amgff){
	    my $gffio = Bio::Tools::GFF->new( -file => $amgff,
	                                   -gff_version => 3);
	                                      
	    my $geneId = "";
	    while(my $feature = $gffio->next_feature()){
	        my $predcount = "0";
	        my $geneid;
	        my $ftype = $feature->primary_tag;
	        if($ftype eq $options{feat_type}){
	               my $attr_href = &process_attrs2($feature);
	               my %attr = %$attr_href;    
	               my @fid = $feature->each_tag_value("ID");
	               $geneid = $fid[0];
	               
	               ++$predcount if exists $attr{'codon'};
	               ++$predcount if exists $attr{'gscan'};
	               ++$predcount if exists $attr{'tcode'};
	        }
	        $am_predcount{$geneid}=$predcount;
	        #print "$geneid\t$predcount\n";
	        
	    }
	}
	print Dumper(\%am_predcount) if $options{debug};
	return (\%am_predcount);
}