#!/depot/perl-5.12.1/bin/perl
#
# Purpose: To conver features in gff to a fasta format.
#
#
#
################################

use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Data::Dumper;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my $usage = qq{
    $0
    -g, --gff
       gff file name
    -m --mapfile
       map file name old ids in column1 and new ids incolumn2
    -t, --feat_type
        Feature type to parse
        Default: all

    --debug
       debug mode
    -h, --help
       this help message
};

my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'mapfile|m=s',
              'feat_type|t=s',
              'debug',
              'help|h',
            );

die("$usage\n") if($options{help});
die("$usage\n") if(!$options{gff});
die("$usage\n") if(!$options{mapfile});

# read in map file
my %map=();
open(READ, $options{mapfile});
while(my $line =<READ>){
	chomp($line);
	my($old, $new) = split("\t", $line);
	$map{$old}=$new;
}
close(READ);
print Dumper(\%map) if $options{debug};

#read in gff
if(-e $options{gff}){
    my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                 -gff_version => 3);

    my %feats = ();my $masterfeat;
    while(my $feature = $gffio->next_feature()){
	my $oldseqid = $feature->seq_id();
	my $newseqid = $map{$oldseqid};
	$newseqid = $oldseqid  if !$newseqid;
    	$feature->seq_id($newseqid);
	my $gffstr = $gffio->gff_string($feature);
        print  $gffstr."\n";
	}

}
