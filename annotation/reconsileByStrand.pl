#!/depot/perl-5.12.1/bin/perl
#
#  Reconsile genes from a GFF using PTU directions....
#  
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'ptu|p=s',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    -g, --gff
        gff
    -p, --ptu
	ptu file
    --prefix
	something to name a file
	Default: prefix 
    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
die("$usage\n") if(!$options{gff});
die("$usage\n") if(!$options{ptu});
$options{prefix} ||= 'prefix';
#die("$usage\n") if(!$options{ids2consider});

## read in ptu file and create a range
my $ptuset = Array::IntSpan->new();
my %ptus = ();
my $contig = "";
open(READ, $options{ptu});
while(<READ>){
	chomp;
	my($contigid, $ptuid, $ptustrand, $ptustart, $ptuend, $ptugenecount)=split("\t");
	$ptus{$ptuid}{contigid}=$contigid;
	$ptus{$ptuid}{ptustrand}=$ptustrand;
	$ptus{$ptuid}{ptustart}=$ptustart;
	$ptus{$ptuid}{ptuend}=$ptuend;
	$ptus{$ptuid}{ptugenecount}=$ptugenecount;
	$contig = $contigid;# for print filenames; assumes gff has only one contig.
	
	#construct ptu range set
	$ptuset->set_range($ptustart, $ptuend, $ptuid);
	
}
print $ptuset if $options{debug};
print Dumper(\%ptus)   if $options{debug};

#my $belongs_to = "Undefined";

#$belongs_to=$set->lookup($pos);
## read in gff
warn "reading GFF\n";

my $gff_href = &hasify_gff($options{gff}, $options{feat_type});
my %gff = %$gff_href;
print Dumper(\%gff)  if $options{debug};

=head
#order geneids per locatio
my %bystart = ();
for my $geneid(sort keys %gff){
	my $fstart = $gff{$geneid}{fmin};
	print $fstart."\n"   if $options{debug};
	#$fstart .= ".0";
	for (1..10){
		next if !exists $bystart{$fstart};
		$fstart += "0.1" if exists $bystart{$fstart};
	}
	$bystart{$fstart}=$geneid;
}
print Dumper(\%bystart)   if $options{debug};

my @geneids_ordby_start;
foreach my $pos (sort {$a <=> $b} keys %bystart){
	push(@geneids_ordby_start, $bystart{$pos});
}
print @geneids_ordby_start   if $options{debug};
=cut
#my ($ptustart, $ptuend, $ptustrand, $featcount) ;

#set output filnames
my $include_file = $contig."_".$options{prefix}."_inRightstrand.gff";
my $exclude_file = $contig."_".$options{prefix}."_inWrongstrand.gff";
my $undecided_file = $contig."_".$options{prefix}."_inUndecided.gff";

open(INCLUDE, ">>$include_file");
open(EXCLUDE, ">>$exclude_file");
open(UNDECIDED, ">>$undecided_file");

foreach my $geneid (sort keys %gff){
	chomp($geneid);
	my $seqid = $gff{$geneid}{fparent};
	my $source = $gff{$geneid}{fsource};
	my $start = $gff{$geneid}{fmin};
	my $end = $gff{$geneid}{fmax};
	my $ftype = $gff{$geneid}{ftype};
	my $score = $gff{$geneid}{fscore};
	my $strand = $gff{$geneid}{fstrand};
	my $phase = $gff{$geneid}{fphase};

	my $belongs2ptu = "Undefined";
	# get ptuid for the gene
	$belongs2ptu=$ptuset->lookup($start);
	$belongs2ptu=$ptuset->lookup($end) if !$belongs2ptu;
	#still does not belog ing any range; may be part of strand switch region
	if(!$belongs2ptu){
		print UNDECIDED "$gff{$geneid}{gffstr}\n";
		next;
	}
	
	my $ptustrand = $ptus{$belongs2ptu}{ptustrand};
	if($strand eq $ptustrand){
		print INCLUDE "$gff{$geneid}{gffstr}\n";
	
	}else{
		print EXCLUDE "$gff{$geneid}{gffstr}\n";
	} 
}
close(INCLUDE);
close(EXCLUDE);


warn "OK\n";
 
 
 
 
 
#----------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------
 
 
sub hasify_gff(){
    my ($gff_files, $type) = shift;
	my %gff = ();                             

    my @files = split(',', $gff_files);
    foreach my $file (@files){
		if(-e $file){
		my $gffio = Bio::Tools::GFF->new( -file => $file,
										  -gff_version => 3);
									  
		my $geneId = "";
		while(my $feature = $gffio->next_feature()){
			my $gffstr = $gffio->gff_string($feature);
			my $ftype = $feature->primary_tag;
			if($ftype eq 'gene'){     
					my @fid = $feature->each_tag_value("ID");
					my $geneid = $fid[0];
					#my @fcolor = $feature->each_tag_value("colour");
					#my $fcolor =  $fcolor[0];
					$gff{$geneid}{"fparent"}=$feature->seq_id;
					$gff{$geneid}{"fmin"}=$feature->start;
					$gff{$geneid}{"fmax"}=$feature->end;
					$gff{$geneid}{"fstrand"}=$feature->strand;
					$gff{$geneid}{"ftype"}=$feature->primary_tag;
					$gff{$geneid}{"fframe"}=$feature->frame;
					$gff{$geneid}{"fsource"}=$feature->source_tag;
					$gff{$geneid}{"fscore"}=$feature->score;
					$gff{$geneid}{"gffstr"}=$gffstr;
				}
			
			}
		}
	} 
	print Dumper(\%gff)  if $options{debug};
        return \%gff;
}
