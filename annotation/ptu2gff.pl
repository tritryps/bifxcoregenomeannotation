#!/depot/perl-5.12.1/bin/perl
#
#  convert ptu file to gff for display purposes
#  
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'ptu|p=s',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
	-p, --ptu
		ptu file
	--prefix
		something to name a file
		Default: prefix 
    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
die("$usage\n") if(!$options{ptu});
$options{prefix} ||= 'prefix';
#die("$usage\n") if(!$options{ids2consider});

## read in ptu file and create a range

open(READ, $options{ptu});
while(<READ>){
	chomp;
	my($contigid, $ptuid, $ptustrand, $ptustart, $ptuend, $ptugenecount)=split("\t");
	my $ptustrand1;
	$ptustrand1 = '-' if $ptustrand eq '-1';
	$ptustrand1 = '+' if $ptustrand eq '1';

	print "$contigid\tPTU\tgene\t$ptustart\t$ptuend\t.\t$ptustrand1\t.\tID=$ptuid;genecount=$ptugenecount;Contig=$contigid;\n";
	
	
	
}

 
 
 
