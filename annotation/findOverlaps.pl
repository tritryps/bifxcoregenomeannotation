#!/depot/perl-5.12.1/bin/perl
#
#  Compares a gff to itself to find if genes are overlapping
#  ignores self overlap
#
##############
use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
          	  'feat_type|f=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    --gff, -g
        GFF file which contains the features to check if they are overlapping to each other in same gff.
 	--feat_type, f
 		The feature type to consider
 		
    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
$options{feat_type} ||= 'gene';

## read in gff
warn "reading GFF\n";

my $gff_href = &hasify_gff($options{gff}, $options{feat_type});
my %gff = %$gff_href;
print Dumper(\%gff)  if $options{debug};

# create ranges for gff
#my $gffrange = &rangify_gff($options{gff}, $options{feat_type});

#create chr-geneid map
my $chr_geneid_map_href = &create_chr_geneid_map($options{gff}, $options{feat_type});

foreach my $chr (keys %gff){
	print "Checking on $chr\n";
	my %data = (); 
	my $gffrange = &rangify_gff($options{gff}, $options{feat_type}, $chr);                               
	foreach my $fid(keys %{$gff{$chr}}){
		
		my $fmin =  $gff{$chr}{$fid}{"fmin"};
		my $fmax =  $gff{$chr}{$fid}{"fmax"};
		my $fstrand = $gff{$chr}{$fid}{"fstrand"};
		
		my $ov_gene=$gffrange->lookup($fmin);
		print "\t$fid\t$ov_gene\n" if $fid ne $ov_gene;
		my $ov_gene=$gffrange->lookup($fmax);
		print "\t$fid\t$ov_gene\n" if $fid ne $ov_gene;	
    }
}
      
my %results = ();


=head
foreach my $algo(keys %predgff){
	chomp($algo);
	print "$algo\t $predgff{$algo}\n" if $options{debug};
	sleep 5;
	my $gffrange = &rangify_gff($algo, $predgff{$algo}, $options{feat_type});
	
	my $am_predcount_href = &count_automagi_preds($predgff{$algo}) if($algo eq 'AUTOMAGI');
	#my %am_predcount = %$am_predcount_href;
	print  Dumper($am_predcount_href) if $options{debug};
	
	foreach my $geneid (sort keys %gff ){
		print $geneid."\t" if $options{debug};
		#sleep 1;
		my %hits =();
		foreach my $pos($gff{$geneid}{fmin}..$gff{$geneid}{fmax}){
			#print "$pos\t";
			my $predgeneid=$gffrange->lookup($pos);
			$hits{$predgeneid}="1" if $predgeneid;
			
		}
		my $ovlcount = keys %hits;
		my $genecount = "0";
		$genecount = 1 if $ovlcount > "0";
		my $genes = join(';', keys %hits);
		print "hits\t$genecount\t$genes\n" if $options{debug};
		# if prediction is from automagi, 
		# use the count of how many of 3 algorithms predicted it.
		if ($algo eq 'AUTOMAGI' && $genecount > "0"){
			my @am_predcounts = (); # to hold counts of algorithms for each of overlapping genes
			# cycle thru each of overlapping automagi gene
			foreach my $predid (keys %hits){
				my $count = $$am_predcount_href{$predid};
				push(@am_predcounts, $count);
			}
			@am_predcounts = sort @am_predcounts;
			$genecount = $am_predcounts[-1];
		}
        $results{$geneid}{$algo}=$genecount;
	    
	}
}

print Dumper(\%results) if $options{debug};

#print results
#print headers
print "GeneId\t";
foreach my $algo (sort keys %predgff){
    print $algo."\t";
}
print "TotalCount\n";

foreach my $geneid (sort keys %results){
	my $tot_count = "0";
	print "$geneid\t";
	foreach my $algo (sort keys %predgff){
		$tot_count += $results{$geneid}{$algo};
		print $results{$geneid}{$algo}."\t";
	}
	print "$tot_count\n";
}
=cut
#----------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------
 
sub create_chr_geneid_map(){
	my($file, $type) = @_;
	my $gffio = Bio::Tools::GFF->new( -file => $file,
                                          -gff_version => 3);
    my %data = ();                                 
    my $geneId = "";
    while(my $feature = $gffio->next_feature()){
    	my $chr = $feature->seq_id;
        my $ftype = $feature->primary_tag;
        if($ftype eq $type){
    	    my ($fid) = $feature->each_tag_value("ID");
         	$data{$fid}=$chr;	    
        }
    }
    print Dumper(\%data) if $options{debug};
    return \%data;
    
}
 
sub hasify_gff(){
    my ($file, $type) = @_;
    my %gff = ();                             

    my $gffio = Bio::Tools::GFF->new( -file => $file,
                                          -gff_version => 3);
                                      
        my $geneId = "";
        while(my $feature = $gffio->next_feature()){
        	my $chr = $feature->seq_id;
            my $ftype = $feature->primary_tag;
            if($ftype eq $type){
            	    my ($fid) = $feature->each_tag_value("ID");
                    $gff{$chr}{$fid}{"fparent"}=$feature->seq_id;
                    $gff{$chr}{$fid}{"fmin"}=$feature->start;
                    $gff{$chr}{$fid}{"fmax"}=$feature->end;
                    $gff{$chr}{$fid}{"fstrand"}=$feature->strand;
                    $gff{$chr}{$fid}{"ftype"}=$feature->primary_tag;
                    
                }
            
            }
        
    	print Dumper(\%gff)  if $options{debug};
        return \%gff;
}

sub rangify_gff(){
    my ($gff_file, $type, $chr) = @_;
    my %gff = ();                             
    my $gffrange = Array::IntSpan->new();
    
    #my @files = split(',', $gff_files);
    if(-e $gff_file){
        my $gffio = Bio::Tools::GFF->new( -file => $gff_file,
                                          -gff_version => 3);
                                      
        my $geneId = "";
        while(my $feature = $gffio->next_feature()){
        	my $current_chr = $feature->seq_id;
        	next if $current_chr ne $chr;
            my $ftype = $feature->primary_tag;
            if($ftype eq $type){
                    my $attr_href = &process_attrs2($feature);
                    my %attr = %$attr_href;    
                    my @fid = $feature->each_tag_value("ID");
                    my $geneid = $fid[0];
                    
                    $gffrange->set_range($feature->start, $feature->end, $geneid);
                }
            
            }
        }
     
    print $gffrange."\n"  if $options{debug};
    return $gffrange;
}

sub process_attrs2{
    my $feat = shift;
    
    my @tags = $feat->get_all_tags();
    
    my $attr;
    my %attr;
    foreach (@tags){
        my $tag = $_;
        my @values = $feat->get_tag_values($tag);
        foreach (@values){
            my $value = $_;
            my $quote_it;
            
            $quote_it++ if $value =~ /\s+/;
            $quote_it++ if $value =~ /;/;
            
            $value = '"'. $value . '"' if $quote_it;
            $attr{$tag} = $value;
        }
        
    }
    
  return \%attr;
}

sub count_automagi_preds(){
	my $amgff = shift;
	my %am_predcount = ();
	
	if(-e $amgff){
	    my $gffio = Bio::Tools::GFF->new( -file => $amgff,
	                                   -gff_version => 3);
	                                      
	    my $geneId = "";
	    while(my $feature = $gffio->next_feature()){
	        my $predcount = "0";
	        my $geneid;
	        my $ftype = $feature->primary_tag;
	        if($ftype eq $options{feat_type}){
	               my $attr_href = &process_attrs2($feature);
	               my %attr = %$attr_href;    
	               my @fid = $feature->each_tag_value("ID");
	               $geneid = $fid[0];
	               
	               ++$predcount if exists $attr{'codon'};
	               ++$predcount if exists $attr{'gscan'};
	               ++$predcount if exists $attr{'tcode'};
	        }
	        $am_predcount{$geneid}=$predcount;
	        #print "$geneid\t$predcount\n";
	        
	    }
	}
	print Dumper(\%am_predcount) if $options{debug};
	return (\%am_predcount);
}