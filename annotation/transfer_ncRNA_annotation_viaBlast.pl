#!/depot/perl-5.12.1/bin/perl

use strict;
use warnings;
use Bio::SeqIO;
use Data::Dumper;
use Bio::SearchIO;
use File::Basename;
use Date::Simple ('date', 'today');
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
umask(0000);



#-------------------------------------------------------------------------------
# handle command line.
#-------------------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'blastfile=s',
              'fasta=s',
              'outfile_base|b=s',
              'source=s',
              'max_con|c=s',
              'max_cov|x=s',
              'max_iden=s',
              'summary|s=s',
              'debug|d=s',
              'help|h');
              
                            

$options{source} ||= 'BLAST';
&blast2hash($options{blastfile});


sub blast2hash(){
    my $oFile = shift;
    my %hResult = ();
    
    #parse blastoutput
     my $in = new Bio::SearchIO( -format => 'blast',
                                 -file   =>  $oFile);
    
    while ( my $result = $in->next_result ){
        my ($queryName, $queryLen) = ($result->query_name, $result->query_length);
            
        #   traverse thru each hit 
        my $hitCount = "0";
        while(my $hit = $result->next_hit){
            
            # parse hit name. The local pdb file has it in the second field of header 
            my ($hitName, $hitLen, $noofHsps)  = ($hit->name, $hit->length, $hit->num_hsps);
            my $hspCount = "0";
            
            #traverse each hsp
            while(my $hsp = $hit->next_hsp){
               ++$hspCount;
               my ($eValue, $fracIden, $fracCons, $gaps) =  ($hsp->evalue, $hsp->frac_identical, $hsp->frac_conserved, $hsp->gaps);                 
               # coverage = (len of query in_HSP/total len of query) *100
               my $covr = sprintf( "%d", 100 * $hsp->length('query') / $result->query_length );
               #if ($fracIden == 1){
               # my $covr = sprintf( "%d", 100 * $hit->length / $result->query_length ) ;
               #}
               my $queryLen_inhsp = $hsp->length('query');
               my ($substart, $subend) = sort {$a <=> $b} ($hsp->start('hit'), $hsp->end('hit'));
               my $hspstrand = $hsp->strand('hit');
               my $substrand;
               $substrand = '-' if $hspstrand eq '-1';
               $substrand = '+' if $hspstrand eq '1';
               
              # print "$queryName\t$hitName\t$queryLen\t$queryLen_inhsp\t$hitLen\t";
              # print "$covr\t$fracIden\t$fracCons\t$eValue\t$substart\t$subend\n";
               my $id = $hitName."-".$queryName."-".$substart;
               
               if ($covr >=50  && $fracIden >= .6){
               			print "$hitName\t$options{source}\tgene\t$substart\t$subend\t.\t$substrand\t.\tID=$id\n";
               }
               #build hash
               $hResult{$hitName}{$hspCount}{"queryName"}= $queryName;
               $hResult{$hitName}{$hspCount}{"queryLen"}= $queryLen;
               $hResult{$hitName}{$hspCount}{"blastOutFile"}=$oFile;
               #$hResult{$hitName}{$hspCount}{"isFirstHsp"}=$isFirstHsp;
               #$hResult{$hitName}{$hspCount}{"isFromFirstHit"}=$isFirstHit;
               $hResult{$hitName}{$hspCount}{"hitCount"}=$hitCount;
               $hResult{$hitName}{$hspCount}{"eValue"}=$eValue;
               $hResult{$hitName}{$hspCount}{"iden"}=sprintf( "%d", ($fracIden * 100));
               $hResult{$hitName}{$hspCount}{"cons"}=sprintf( "%d", ($fracCons * 100));
               $hResult{$hitName}{$hspCount}{"covr"}=$covr;
               #$hResult{$hitName}{$hspCount}{"covr2"}=$covr2;
                                              
               #$isFirstHsp = "NO";
            }#while(my $hsp = $hit>next_hsp)
        
        #$isFirstHit ="NO";
        ++$hitCount;
        }#while(my $hit = $result->next_hit)
        
    }#while ( my $result = $in->next_result )
    return \%hResult;            
}
