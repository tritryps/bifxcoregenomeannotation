#!/depot/perl-5.12.1/bin/perl
#
#  Adds a tag value pair to a gff feature. Multiple tags can be added too. Table needs to follow the a strict format. 
# First column is always the ID of the feature with heading ID. 
# second column is the value for the first tag-value pair. And the header of the column is the tag.
# third column is the value for the second tag-value pair. And the header of the column is the tag.
# and so on.....
# Header column should always start with 
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use URI::Escape;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'tags=s',
              'valuefile=s',
              'overwrite',
              'prefix=s',
              'add_name_tag',
              'common_tagvalues=s',
              'feat_type|f=s',
              'print_all_features',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    -g, --gff
        Any GFF file
        Might work with any GFF with different sources
    -f, --feat_type
        Type of the future to add the tags to
    -valuefile
    	Tag values. First column is the feature ID, rest of the columns should have values.
    	Order of tags in '-tags' option should be same as the value columns
    -tags
    	Comma separated tag names
    	Keep the order of values and tag same
    -overwrite
        If this is set, any new tags added will replace the existing content of the SAME tag
        Tags that are NOT added will not be affected.
    -add_name_tag
        Add name tag to all features (same as ids)
    -common_tagvalues
        Anything that needs to be added to all the changed features (for the ones present in valuefile)
        tag1=value1;tag2=value2;tag3=value3
        
    -print_all_features
    	when set, all the features that are not matching to options{feat_type} will be printed as such.
        
    --prefix
        something to name a file
        Default: prefix 

    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
die("$usage\n") if(!$options{gff});
die("$usage\n") if(!$options{tags});
die("$usage\n") if(!$options{valuefile});
$options{prefix} ||= $options{gff};
$options{prefix} =~ s/\.gff.*//;
$options{feat_type} ||= 'gene';

#create outfile names and write handles
my $gff_base = $options{gff};
$gff_base =~ s/\.gff$//;
my $gff_annotation_appended = $gff_base.".annotation_appended.gff";
my $gff_annotated_only = $gff_base.".annotated_only.gff";
my $gff_not_annotated = $gff_base.".not_annotated.gff";
open(APPEND, ">>$gff_annotation_appended");
open(ANNOTONLY, ">>$gff_annotated_only");
open(NOANNOT, ">>$gff_not_annotated");



my %tagvalues = ();
my %common_tagvalues = ();
&hasify_common_tagvalues($options{common_tagvalues}) if ($options{common_tagvalues});
&hasify_tagvalues($options{tags}, $options{valuefile});

#print header
my $header = q{##gff-version	3
##feature-ontology	so.obo
##attribute-ontology	gff3_attributes.obo
};
print APPEND $header;
print ANNOTONLY $header;
print NOANNOT $header;

#read in gff
if(-e $options{gff}){
    my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                 		-gff_version => 3);

    #my @ftypes = ('gene', 'mRNA', 'exon', 'CDS');
    #my %feats = ();my $masterfeat;
    while(my $feat = $gffio->next_feature()){
        my ($fid) = $feat->get_tag_values("ID");
        my $ori_ftype = $feat->primary_tag();
        
        #get all the tag values for this gene...
        my $gffstr = $gffio->gff_string($feat);
        print APPEND $gffstr."\n"  if !exists $tagvalues{$fid};
        print NOANNOT $gffstr."\n"  if !exists $tagvalues{$fid};
        next if !exists $tagvalues{$fid};
        
        my $valueset_href = $tagvalues{$fid};
        #my %valueset = %$valueset_href;
        print Dumper($valueset_href) if $options{debug};
        
    
        if($ori_ftype eq $options{feat_type}){
            foreach my $tag(keys %$valueset_href){
	            $feat->remove_tag($tag) if ($feat->has_tag($tag) && $options{overwrite});
	            my $value = $$valueset_href{$tag};
	            
	            #trim spaces at front and back
                $value =~ s/^\s+//; $value =~ s/\s+$//;
                #$value = uri_unescape($value);
	            $value = &format_productname($value) if $tag eq 'product';
	            
	            $value = '"'.$value.'"' if ($value !~ /^\"/ && $value !~ /^\"$/);
	            $feat->add_tag_value($tag, $value);    	
	        }
	        my $gffstr = $gffio->gff_string($feat);
            print APPEND $gffstr."\n";
            print ANNOTONLY $gffstr."\n";
        }else{
            my $gffstr = $gffio->gff_string($feat);
            print APPEND $gffstr."\n";
            print NOANNOT $gffstr."\n";
            print ANNOTONLY $gffstr."\n" if $options{print_all_features};
        }
        
    }                                  
}
close(APPEND);
close(ANNOTONLY);
close(NOANNOT);


sub format_productname(){
    my $name = shift;
    $name =~ s/^\"//;$name =~ s/^\'//;
    $name =~ s/\"$//;$name =~ s/\'$//;
    
    if($name =~/hypothetical protein, unknown function/){
        $name = "hypothetical protein, conserved";
    }
    if($name !~ /putative/){
        if($name !~ /hypothetical/){
            $name .= ", putative";
            
        }
    }
    return $name;
}

sub hasify_tagvalues(){
    my($tagline, $valuefile) = @_;
    my @tags = split(',', $tagline);
    
    open(READ, $valuefile);
    while(my $line = <READ>){
        chomp($line);
        next if $line =~ /^#/;
        my @values = split('\t', $line);
        die( "Number of tags and values do NOT match\n") if $#tags != $#values-1;  
        foreach my $index (0..$#tags){
            $tagvalues{$values[0]}{$tags[$index]} = $values[$index+1];
        }
        if ($options{common_tagvalues}){
          foreach my $key (keys %common_tagvalues){
               $tagvalues{$values[0]}{$key} = $common_tagvalues{$key};
          }
        }
    }
    print Dumper(\%tagvalues) if $options{debug};
}

sub hasify_common_tagvalues(){
	my $line = shift;
        foreach my $pair (split(',', $line)){
           chomp($pair);
           next if $pair !~ /=/;
           my($key, $value) = split('=', $pair);
           $common_tagvalues{$key}=$value;  
        }
        print Dumper(\%common_tagvalues) if $options{debug};

}


