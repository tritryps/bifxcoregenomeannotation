#!/depot/perl-5.12.1/bin/perl
#
# Adds lengh=xxx tag to each feature. length is calculated using fmax-fmin+1 formula
# 
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Math::Round;
use Data::Dumper;
use URI::Escape;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'overwrite',
              'feat_type|f=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    -g, --gff
        Any GFF file
        Might work with any GFF with different sources
    -f, --feat_type
        Type of the future to add the tags to
    -overwrite
        If this is set, any new tags added will replace the existing content of the SAME tag
        Tags that are NOT added will not be affected.

    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
die("$usage\n") if(!$options{gff});
$options{prefix} ||= $options{gff};
$options{prefix} =~ s/\.gff.*//;
$options{feat_type} ||= 'gene';

#print header
my $header = q{##gff-version	3
##feature-ontology	so.obo
##attribute-ontology	gff3_attributes.obo
};
print $header;

#read in gff
if(-e $options{gff}){
    my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                 		-gff_version => 3);

    while(my $feat = $gffio->next_feature()){
        my ($fid) = $feat->get_tag_values("ID");
        my $ori_ftype = $feat->primary_tag();
        if($ori_ftype eq $options{feat_type}){
			my $tag = "length";
        	my $length = 0;
        	$length = $feat->end() - $feat->start() + 1;
		$length = $length - 15;
		$length = 0 if $length < 0;
        	$feat->remove_tag($tag) if ($feat->has_tag($tag) && $options{overwrite});
         	$feat->add_tag_value($tag, $length);
         	my $gffstr = $gffio->gff_string($feat);
         	print  $gffstr."\n";
        }else{
            my $gffstr = $gffio->gff_string($feat);
            print  $gffstr."\n";
        }
        
    }                                  
}

