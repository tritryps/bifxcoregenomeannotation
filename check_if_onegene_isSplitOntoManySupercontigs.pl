#!/depot/perl-5.12.1/bin/perl
#
#  Checks if the split gff (oputout of chromosome2supercontig.pl) contains any 
#  feature that is overlapping two suerpcontigs. this is important for the genes that is 
#  predited across gap region/superconting fusion region plus have two exons one on each supercontig.
#  In this case genes go into one. but each exons and cds go to both contigs
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'feat_type|f=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    -g, --gff
        gff file idealy output of chromosome2supercontig.pl script (via agp file)
    -f, --feat_type
        Type of the future to match and convert from
    
    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
die("$usage\n") if(!$options{gff});

my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                 -gff_version => 3);


my %data = ();

while(my $feature = $gffio->next_feature()){
    my ($fid)= $feature->get_tag_values("ID") if $feature->has_tag('ID') ;
    my ($parent)= $feature->get_tag_values("Parent") if $feature->has_tag('Parent');
    my $ftype = $feature->primary_tag;
    my $contig = $feature->seq_id;
    
    $parent =~ s/-mRNA-1// if $ftype eq "mRNA";
    $parent =~ s/-CDS-1//  if $ftype eq "CDS";
    $fid = $parent if ($ftype ne "gene");
    if (exists $data{$fid}){
        if($contig ne $data{$fid}){
        	print "$fid => $contig,$data{$fid}\n";
        }
    }else{
        $data{$fid}=$contig;
    }
    
}                                
                                 
