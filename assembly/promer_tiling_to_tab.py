#!/usr/bin/python
__author__ = 'mcobb'

import re
import csv
import sys
import fileinput


def readFile():
    print "Scaffold \t Chr \t Order \t Dir \t Start \t End \t Gap \t Scaff_len \t Scaff_Cov \t % Idy"
    for lines in fileinput.input():
        # Checks for the header line. If header, collects chromosome. sets the iterator back to 0.
        if lines.startswith('>'):
            chromosome = re.split("\.(.*)\s.*\s", lines)[0]
            chromosome = chromosome.split(" ")[0]
            order = 1
        else:
            entry = lines.split()
            start = entry[0]
            end = entry[1]
            gap = entry[2]
            length = entry[3]
            coverage = entry[4]
            identity = entry[5]
            dir = entry[6]
            scaffold = entry[7]
            print scaffold + '\t' + chromosome + '\t' + str(order) + '\t' + dir + '\t' + start + '\t' + end + '\t' + gap + '\t' + length + '\t' + coverage + '\t' + identity
            order += 1


def main():
    readFile()


main()
