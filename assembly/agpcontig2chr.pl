#!/depot/perl-5.12.1/bin/perl
#
# Purpose: to convert supercontigs to chr using agp
#
#
################################
use Bio::DB::Fasta;
use Bio::Seq;
use Bio::SeqIO;

open(AGP,"/nearline/ngs/projects/Beverley/CfaC1_MergeSupercontig/CfaC1.v13.3.5.pseudochr.assembly.agp") or die $!;
my @chr = ();

my $db = Bio::DB::Fasta->new("/nearline/ngs/Dropbox/CrithidiaAnnotation/assemblyV13.3.5_annotationV0.6/CfaC1.v13.3.5.supercontigs.fasta");

while(<AGP>){
	chomp;
	split /\s/;
	# extend temp string if it's too short
	do{$chr[$_[0]] .= ' ' x 1_000_000;}while length $chr[$_[0]] < $_[2] ;
	if($_[4] !~ m/N/){
		($start,$stop) = $_[8] eq '+'?($_[6], $_[7]):($_[7], $_[6]);
		$s = substr $chr[$_[0]], $_[1], $_[9], $db->seq($_[5],$start,$stop);
	}else{
		$s = substr $chr[$_[0]], $_[1], $_[5], "N" x $_[5] ;
	}
}

#remove any trailing whitespace
@chr = map{s/\s+//g;$_}@chr;

#print the sequence. chromosomes are chr0 -> chr8
foreach(0..$#chr){
    my $seqobj = Bio::Seq->new( -display_id => "chr$_", -seq => $chr[$_]);
    my $seq_out = Bio::SeqIO->new('-file' => ">chr$_.fa",'-format' => 'fasta');
    $seq_out->write_seq($seqobj);
}
