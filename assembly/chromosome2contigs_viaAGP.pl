#!/depot/perl-5.12.1/bin/perl
#
#  Split features on chromosome onto contigs using AGP/GFF file
#
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'agp|a=s',
              'fasta|f=s',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0
    -g, --gff
        gff
        
    -f, --fasta
    	fasta file    

    -a, --agp
		AGP file
    
    --prefix
		something to name a file
		Default: prefix
	
		
    -h, --help
    -d, --debug
};

print $usage if $options{help};
my $agphref = &hasify_agp($options{agp});
print Dumper($agphref) if $options{debug};
my %agph = %$agphref;

my $fasta_href = &hasify_fasta($options{fasta});
print Dumper($fasta_href)  if $options{debug};
my %fasta = %$fasta_href;






#my $pseudochr_fasta_href = &stich_pseudochr($options{fasta}, $options{agp});


#walk thru gff which contain features that needs to be remapped on chromosomes
my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
									  -gff_version => 3);

while(my $feature = $gffio->next_feature()){
	my $gffstr = $gffio->gff_string($feature);
	my $ftype = $feature->primary_tag;
	my $fchr = $feature->seq_id;
	my $fmin = $feature->start;
	my $fmax = $feature->end;
	my $fstrand = $feature->strand;
	my $fframe = $feature->frame;
	my $fsource = $feature->source_tag;
	my $fscore = $feature->score;
	my $contigid = $feature->seq_id;
	
	next if $ftype eq 'contig';
	next if $ftype eq 'supercontig';
	next if $fchr =~ /CfaC1.00/;
	
	my $contig_st_inpseudo  = $agph{$contigid}{sust};
	my $contig_end_inpseudo = $agph{$contigid}{suend};
	my $pseudochr_id = $agph{$contigid}{suid};
	$feature->seq_id($pseudochr_id);
	if($agph{$contigid}{fragdir} eq '+'){
		my $newfmin = $fmin + $contig_st_inpseudo - 1;
		my $newfmax = $fmax + $contig_st_inpseudo - 1;
		$feature->start($newfmin);
		$feature->end($newfmax);
	}elsif($agph{$contigid}{fragdir} eq '-'){
		my $newstrand;
		$newstrand = '-1' if $fstrand eq '1';
		$newstrand = '1'  if $fstrand eq '-1';
		$feature->strand($newstrand);
		my $newfmin; my $newfmax;
		$newfmin = $contig_end_inpseudo - $fmin + 1;
		$newfmax = $contig_end_inpseudo - $fmax + 1;
		($newfmin, $newfmax) = sort {$a<=>$b} ($newfmin, $newfmax);
		$feature->start($newfmin);
		$feature->end($newfmax);
	}
	my $newgffstr = $gffio->gff_string($feature);
	
	print "$newgffstr\n";
	

}

print "##FASTA\n";
my $pseudochr_fasta_href = &stich_pseudochr($options{fasta}, $options{agp});
sub stich_pseudochr(){
	my ($fastafile, $agpfile) = @_;
	my %fasta = ();
	my $fastaio = Bio::SeqIO->  new( -file => $fastafile,
                                    -format => 'fasta' ) or die "Fail reading Fasta file : $fastafile\n";
    while(my $seq = $fastaio->next_seq()){
        my $id = $seq->id;
        my $residues = $seq->seq();
        $residues =~ s/\*//;
        my $len = length($residues);
        $fasta{$id}{seq}=$residues;
        $fasta{$id}{len}=$len;

    }
    #print Dumper(\%fasta);
    my %agph = ();
	my %agph2 = ();

	my $dna;
	my $prevsu = "TEST";
	open(READ, $agpfile);

	while(my $line = <READ>){
		chomp($line);
		my($su, $sust, $suend, $fragorder, $fragtype) = split("\t", $line);
		#print if start of next pseudochr
		if($fragorder eq '1'){
			print ">$prevsu\n$dna\n";
			$dna = "";
		}
		if($fragtype ne 'N'){
		my($su, $sust, $suend, $fragorder, $fragtype, $fragid, $fragst, $fragend, $fragdir) = split("\t", $line);
		
		warn $line."\n";
		warn "$fragdir\n";
		warn "well\n";
		
		$dna .= $fasta{$fragid}{seq} if $fragdir eq '+';
		$dna .= revcomp($fasta{$fragid}{seq}) if $fragdir eq '-';
		}elsif($fragtype eq 'N'){
		my($su, $sust, $suend, $fragorder, $fragtype, $fraglen, $fragsource, $no) = split("\t", $line);
			foreach (1..$fraglen){
				$dna .= 'N';
			}
		}	
		$prevsu = $su
	}
	print ">$prevsu\n$dna\n";
 

}

sub revcomp(){
	my $seq = shift;
	$seq =~ s/\s//g;
	my $dna = reverse($seq);
	$dna =~ tr/ATGCNatgcn/TACGNtacgn/;
	return $dna;
}


#----------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------

sub hasify_fasta(){
    my $file = shift;
    # Read and hasify fasta
    my %fasta = ();
    my $fastaio = Bio::SeqIO->  new( -file => $file,
                                    -format => 'fasta' ) or die "Fail reading Fasta file : $file\n";
    while(my $seq = $fastaio->next_seq()){
        my $id = $seq->id;
        my $residues = $seq->seq();
        $residues =~ s/\*//;
        my $len = length($residues);
        $fasta{$id}{seq}=$residues;
        $fasta{$id}{len}=$len;

    }
    return (\%fasta);
}


sub create_intspan_forchr(){
	my($agpfile, $chr) = @_;
	my $chr_contig_range = Array::IntSpan->new();
	
	open(READ, $agpfile);
	while(my $line = <READ>){
		chomp($line);
		next if $line =~ /fragment/;
		my($su, $sust, $suend, $fragorder, $fragtype, $fragid, $fragst, $fragend, $fragdir) = split("\t", $line) ;
		$su =~ s/susu_/CfaC1\./;
		next if $su ne $chr;
		#print "Making range for $su : $sust $suend $fragid\n";
		$chr_contig_range->set_range($sust, $suend, $fragid) if $fragtype ne 'N';
	}
	return($chr_contig_range);


}

sub hasify_agp(){
	my($agpfile) = @_;
	my %agph = ();
	my %agph2 = ();

	open(READ, $agpfile);
	while(my $line = <READ>){
		chomp($line);
		next if $line =~ /fragment/;
		my($su, $sust, $suend, $fragorder, $fragtype, $fragid, $fragst, $fragend, $fragdir) = split("\t", $line);
		
		#$agph2{$su}{$fragorder}
		
		
		$agph{$fragid}{fragid}=$fragid;
		$agph{$fragid}{sust}=$sust;
		$agph{$fragid}{suend}=$suend;
		$agph{$fragid}{fragtype}=$fragtype;
		$agph{$fragid}{suid}=$su;
		
		
		if($fragtype ne 'N'){
			$agph{$fragid}{fragst}=$fragst;
			$agph{$fragid}{fragend}=$fragend;
			$agph{$fragid}{fragdir}=$fragdir;
		}
		
		
		
		
	}
	return(\%agph);
}


sub hasify_gff(){
    my ($file) = @_;
    my %gff = ();
    if(-e $file){
	
	my $gffio = Bio::Tools::GFF->new( -file => $file,
									  -gff_version => 3);
	my $geneId = "";
	
	while(my $feature = $gffio->next_feature()){
		my $gffstr = $gffio->gff_string($feature);
		my $ftype = $feature->primary_tag;
		if($ftype eq 'gene'){
			my @fid = $feature->each_tag_value("ID");
			my $geneid = $fid[0];
			$gff{$geneid}{"fparent"}=$feature->seq_id;
			$gff{$geneid}{"fmin"}=$feature->start;
			$gff{$geneid}{"fmax"}=$feature->end;
			$gff{$geneid}{"fstrand"}=$feature->strand;
			$gff{$geneid}{"ftype"}=$feature->primary_tag;
			$gff{$geneid}{"fframe"}=$feature->frame;
			$gff{$geneid}{"fsource"}=$feature->source_tag;
			$gff{$geneid}{"fscore"}=$feature->score;
			$gff{$geneid}{"gffstr"}=$gffstr;
	 	}
	}
}
}	
