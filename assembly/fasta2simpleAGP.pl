#!/depot/perl-5.12.1/bin/perl
#
#   Note, this script does not create a perfect AGP file. For that use a diff script.
#   This only creates a rough agp file that need to hand edited.
#   No N runs are added in between    
#
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'fasta|f=s',
              'mapfile|m=s',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0
    -f, --fasta
        fasta with all contigs
    -m, --mapfile
        file with mappingids
        two columns, first=>ids as in fasta second=>new/supercontig ids
        Note, this script does not create a perfect AGP file. For that use a diff script.
        This only creates a rough agp file that need to hand edited.
        
        For genes with out an entry in mapfile, original  fasta ids will be used    
        
    --prefix
        something to name a file
        Default: prefix
    
        
    -h, --help
    -d, --debug
};

die($usage) if $options{help};
#hasify mapfile

my $map_href = &hasify_mapfile($options{mapfile});
#hasify fasta
my $fastahref = &fasta2hash($options{fasta});
my %fastah = %$fastahref;

foreach my $contig(keys %fastah){
	my $newid = $contig;
	my $len = length($fastah{$contig});
	
	$newid  = $$map_href{$contig} if exists $$map_href{$contig};
	print "$newid\t1\t$len\t1\tF\t$contig\t1\t$len\t+\n";
}

#----------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------

sub hasify_mapfile(){
	my $file = shift;
	my %map = ();
	open(READ, $file);
	while(my $line = <READ>){
		chomp($line);
		my($id1, $id2) = split("\t", $line);
		$map{$id1}=$id2;
	}
	print Dumper(\%map) if $options{debug};
	return (\%map);
}

sub revcomp(){
    my($seq) = @_;
    $seq = reverse($seq);
    #$seq = uc($seq);
    $seq =~ tr/ATGCNatgcn/TACGNtacgn/;
    
    return $seq;
}

sub fasta2hash(){
    my $fastafile = shift;
    my %fastahash = ();
    my $seq_in = Bio::SeqIO->new(-file   => $fastafile,
                                 -format => "fasta" );
    while (my $inseq = $seq_in->next_seq) {
        my $seqid = $inseq->id;
        my $seq = $inseq->seq();
        $seq = uc($seq);
        $fastahash{$seqid}=$seq;   
    }
    return (\%fastahash);
}