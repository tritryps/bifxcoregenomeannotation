#!/depot/perl-5.12.1/bin/perl
#
#   merge contigs to chr suing agp file v2
#
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);

my $opt_obj = GetOptions (\%options,
              'fasta|f=s',
              'agp|a=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0
    -f, --fasta
        fasta
    -a, --agp
    	AGP file V2.0

        
    -h, --help
    -d, --debug
    --example
	
};


print $usage if $options{help};

#hasify fasta
my $fastahref = &fasta2hash($options{fasta});
my %fastah = %$fastahref;

my $agphref = &hasify_agp($options{agp});
print Dumper($agphref) if $options{debug};
my %agph = %$agphref;

foreach my $su (keys %agph){
	my $chrseq = "";
	print "Working on $su\n" if $options{debug};
	foreach my $fragorder (sort {$a<=>$b} keys %{$agph{$su}}){
		
			
		my $contigid = $agph{$su}{$fragorder}{fragid};
		if($agph{$su}{$fragorder}{fragtype} ne 'N'){
			my $contigseq = $fastah{$contigid};
			$contigseq = &revcomp($contigseq) if $agph{$su}{$fragorder}{fragdir} eq '-';
			print "$su => $fragorder =>  $contigid => $contigseq\n" if $options{debug};
			$chrseq .= $contigseq;
		}else{
			my $gapseq = &generate_runseq('N', $agph{$su}{$fragorder}{gaplen});
			$chrseq .= $gapseq;
		}
	}
	print ">".$su."\n".$chrseq."\n";
}







	
	


#----------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------
sub revcomp(){
	my($seq) = @_;
	$seq = reverse($seq);
	#$seq = uc($seq);
	$seq =~ tr/ATGCNatgcn/TACGNtacgn/;
	
	return $seq;
}

sub generate_runseq(){
	my($chr, $len) = @_;
	my $str;
	foreach my $times (1..$len){
		$str .= $chr;
	}
	return $str;
}



sub fasta2hash(){
    my $fastafile = shift;
    my %fastahash = ();
    my $seq_in = Bio::SeqIO->new(-file   => $fastafile,
                                 -format => "fasta" );
    while (my $inseq = $seq_in->next_seq) {
        my $seqid = $inseq->id;
        my $seq = $inseq->seq();
        $seq = uc($seq);
        $fastahash{$seqid}=$seq;   
    }
    return (\%fastahash);
}


sub hasify_agp(){
	my($agpfile) = @_;
	my %agph = ();

	open(READ, $agpfile);
	while(my $line = <READ>){
		chomp($line);
		#next if $line =~ /fragment/;
		my($su1, $sust1, $suend1, $fragorder1, $fragtype1) = split("\t", $line);
		if($fragtype1 ne 'N'){
			my($su, $sust, $suend, $fragorder, $fragtype, $fragid, $fragst, $fragend, $fragdir) = split("\t", $line);
			$agph{$su}{$fragorder}{fragid}=$fragid;
			$agph{$su}{$fragorder}{sust}= $sust;
			$agph{$su}{$fragorder}{suend}= $suend;
			$agph{$su}{$fragorder}{fragtype}= $fragtype;
			$agph{$su}{$fragorder}{fragorder}= $fragorder;
			$agph{$su}{$fragorder}{fragst}= $fragst;
			$agph{$su}{$fragorder}{fragend}= $fragend;
			$agph{$su}{$fragorder}{fragdir}= $fragdir;
		}else{
			my($su, $sust, $suend, $fragorder, $fragtype, $gaplen, $gaptype, $islinked, $linkevi) = split("\t", $line);
			$agph{$su}{$fragorder}{sust}= $sust;
			$agph{$su}{$fragorder}{suend}= $suend;
			$agph{$su}{$fragorder}{fragtype}= $fragtype;
			$agph{$su}{$fragorder}{gaplen} = $gaplen;
			$agph{$su}{$fragorder}{gaptype} = $gaptype;
			$agph{$su}{$fragorder}{islinked} = $islinked;
			$agph{$su}{$fragorder}{linkevi} = $linkevi;
			$agph{$su}{$fragorder}{fragid}='GAP';
		}
		
	}
	return(\%agph);
}
