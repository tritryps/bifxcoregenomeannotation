#!/usr/bin/perl

=head1 NAME  
  
  agpcontig2chr.pl

=head1 DESCRIPTION

  convert GFF locations from contig to chromosome/scaffold using
  AGP files (e.g. NCBI genomes Assembled_chromosomes.
  
=head2 GetOptions

  "agp=s" => \$newagp,
  "dirnew=s" => \$dirnew,
  "gff=s" => \@gff,
  "verbose!" => \$verbose,

=head1 AUTHOR
  
  don gilbert, gilbertd at indiana edu
  
=head1 SEE ALSO

  convert GFF from oldAGP to newAGP
  http://eugenes.org/gmod/genogrid/drospege/agpliftgff.pl

=cut  

use strict;
use Getopt::Long;
use Data::Dumper;

my $verbose = 1;
my $newagp= undef;
my $dirnew= undef;
my @gff= ();

my $ok= GetOptions( 
  "agp=s" => \$newagp,
  "dirnew=s" => \$dirnew,
  "gff=s" => \@gff,
  "verbose!" => \$verbose,
  );

push(@gff, @ARGV);
die "usage: agpctg2chr -dirnew newgff -agp table.agp contigs.gff \nperldoc $0 for help \n"
  unless($ok && $newagp && @gff);

if($dirnew) {
  $dirnew =~ s,/$,, if($dirnew);
  unless(-d $dirnew) { warn "mkdir $dirnew\n"; mkdir($dirnew); }
}

my $agp= readAgp($newagp);

foreach my $gff (@gff) {
  (my $gffnew= $gff) =~ s/\.gz//; 
  if($dirnew) { $gffnew =~ s,^.*/,$dirnew/,; }
  else { $gffnew.=".new"; }
  ctg2chrByAgp($agp,$gff,$gffnew);
}


#--------------------------------------------------------------------------


# index agp row [] array
use constant SCAFID => 0;
use constant SCAF_B => 1;
use constant SCAF_E => 2;
use constant CTGID  => 5;
use constant CTG_B  => 6;
use constant CTGLEN => 7;
use constant CTG_OR => 8;

sub readAgp {
  my($agpfile)= @_;
  my %agp; 
  my($ngap,$ncontig);
  if($agpfile =~ /\.gz$/ && -f $agpfile) { open(F,"gunzip -c $agpfile|") or die "agp file bad"; }
  else { open(F,$agpfile) or die "cant read $agpfile"; }
  while(<F>){
    next unless(/^\w/); chomp;
    my @v= split"\t";
    # WN_TYPE == W,N,F,... what all are these codes?
    
    my ($scafid, $scaf_b, $scaf_e, $agpi, $WN_TYPE, 
        $ctgid, $ctgstart, $ctglen, $gaps, $FRAGMENT, $YES, $PLUS);

    if($v[4] eq 'N' || $v[6] eq 'fragment') {
      ($scafid, $scaf_b, $scaf_e, $agpi, $WN_TYPE, $gaps, $FRAGMENT, $YES)= @v;
      $agpi--; # 0-origin
      my $agpr= [ $scafid, $scaf_b, $scaf_e, $agpi, $WN_TYPE, $gaps, 0, ];
      $agp{$scafid}[$agpi]= $agpr;
      $ngap++;

    } elsif($v[4] =~ /W|F/) {
      ($scafid, $scaf_b, $scaf_e, $agpi, $WN_TYPE, $ctgid, $ctgstart, $ctglen, $PLUS)= @v;
      $agpi--; # 0-origin
      my $agpr= [ $scafid, $scaf_b, $scaf_e,  $agpi, $WN_TYPE, $ctgid, $ctgstart, $ctglen, $PLUS, 0 ];
      $agp{$scafid}[$agpi]= $agpr;
      $agp{$ctgid}= $agpr; # for crossref by ctgid
      $ncontig++;
      
    } else {
      warn "Error unknown line: $_\n"; next;
    }
  }
  #print Dumper(\%agp);

  warn "# read $agpfile: n_scaf=",scalar(keys(%agp))," n_contig=",$ncontig," ngap=",$ngap,"\n" if ($verbose);
  return \%agp;
}


sub offsetBy {
  my($agpr,$ref,$start,$stop, $orient)= @_;
  my $refnew= $$agpr[SCAFID];
  if($ref eq $refnew) { return($ref, $start, $stop, $orient); }  
  if($ref ne $$agpr[CTGID]) {
    warn "# contig ID mismatch: $ref ne $$agpr[CTGID]\n" ;
    return($ref, $start, $stop, $orient);
    }
  my $ctgor= $$agpr[CTG_OR];
   my ($nstart, $nstop, $norient);  

  if($ctgor eq "-") { # reversed orient
  print "HERE:$$agpr[CTG_B] + $$agpr[CTGLEN] + $$agpr[SCAF_B]\n";
  print "INPU:$start, $stop, $orient\n"; 
    my $ctg_e = $$agpr[CTG_B] + $$agpr[CTGLEN] ;
    $nstart = $ctg_e - $stop  + $$agpr[SCAF_B];
    $nstop  = $ctg_e - $start + $$agpr[SCAF_B]; 
    $norient= ($orient eq "-") ? "+" : "-";
    print "OUTP:$nstart, $nstop, $norient\n"; 

  } else {
    $nstart = $start - $$agpr[CTG_B] + $$agpr[SCAF_B];
    $nstop  = $stop  - $$agpr[CTG_B] + $$agpr[SCAF_B]; 
  }
  return ($refnew ,$nstart, $nstop, $norient);
}


sub ctg2chrByAgp {
  my($agp,$gff,$gffnew)= @_;
  my ($nchange,$nread,$ok);
 
  #print Dumper($agp);
  warn "# ctg2chrByAgp $gff\n" if ($verbose);
  if($gff =~ /\.gz/ && -f $gff) { $ok= open(F,"gunzip -c $gff|"); } 
  else { $ok= open(F,$gff); }
  die "cant read $gff" unless ($ok);
  open(OUT,">$gffnew") or die "cant write $gffnew";
  my $outh= *OUT;
  
  while(<F>){
    unless(/^\w/){ print $outh $_; next; }
    my($ref,$gffsource,$type,$start,$stop,$eval,$strand,$offs,$attr)= split"\t";
    my ($nref,$nstart,$nstop,$nstrand)= ($ref,$start,$stop,$strand);
    warn "$nref,$nstart,$nstop,$nstrand\n";
    my $agpr = $agp->{$ref};  ## ref here is CONTIG id
    warn $agpr."\n";
    if(defined $agpr) {
      ($nref,$nstart,$nstop,$nstrand)= offsetBy($agpr,$ref,$start,$stop,$strand);
    } else {
      warn "# cant find $ref:$start,$stop\n" ;
    }

    $nread++;
    $nchange++ if($nstart != $start || $nstop != $stop);
    #print $outh join("\t", $nref,$gffsource,$type,$nstart,$nstop,$eval,$nstrand,$offs,$attr);
    print join("\t", $nref,$gffsource,$type,$nstart,$nstop,$eval,$nstrand,$offs,$attr);
    
    if($verbose && $nread % 1000 == 0){ print STDERR "."; print STDERR "\n" if($nread % 50000 == 0); }
  }
  close(F); close(OUT);
  warn "\n# wrote $gffnew: nchange=",$nchange," nread=",$nread,"\n" if ($verbose);
}



__END__

# AGP OLD
scaffold_1696	1	2098	1	W	contig_1918	1	2098	+
scaffold_1696	2099	2130	2	N	32	fragment	yes
scaffold_1696	2131	7778	3	W	contig_1919	1	5648	+
scaffold_1696	7779	7803	4	N	25	fragment	yes
scaffold_1696	7804	12220	5	W	contig_1920	1	4417	+

# AGP NEW
scaffold_1696	1	2098	1	W	contig_1918	1	2098	+
scaffold_1696	2099	2123	2	N	25	fragment	yes	    ** lost 7
scaffold_1696	2124	7771	3	W	contig_1919	1	5648	+
scaffold_1696	7772	7839	4	N	68	fragment	yes	    ** gained cum. 36
scaffold_1696	7840	12256	5	W	contig_1920	1	4417	+

