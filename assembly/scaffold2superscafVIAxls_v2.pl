#!/depot/perl-5.12.1/bin/perl
#
#  Stiches the contigs together to make sueper(-super)contigs using a map file
#  map file could be dumpted excel sheet (Peter) or (promer)
#  
#  
#
##############
use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'mapfile|m=s',
              'fasta|f=s',
              'insert_size=s',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    --mapfile, -m
        Map file with information on contigs to be linked(dumped from excell).
    --fasta, -f
        Fasta file with contigs
    --insert_size
        Size of insert "N"s between scaffolds
    --prefix
        something to name a file
        Default: prefix 
     -h, --help
    -d, --debug
};

$options{insert_size} ||= "100";
die("$usage\n") if($options{help});
die("mapfile options is must.\n\n$usage\n") if(!$options{mapfile});
die("fasta options is must.\n\n$usage\n") if(!$options{fasta});

## prepare N insert
my $insert = "";
for(1..$options{insert_size}){
	$insert .= "N";
}
warn "Insert used to separate scaffolds:". $insert. "\n";


## read contig file to memory/hash
my $contigs_href = &fasta2hash($options{fasta});
#print Dumper($contigs_href) if $options{debug};



## read in mapfile
warn "reading mapfile\n";
my %map =();
my $ordinal = "0";
open(MAP, "$options{mapfile}");
while(my $line = <MAP>){
	next if($line =~ /^#/);
	my @fields = split("\t", $line);
	my $parentchr = $fields[1];
	my $pairedchr = $fields[0];
	my $childcontig = $fields[10];
	my $childorientation = $fields[9];
	warn "$childcontig does not have a orientation\n" if !$childorientation;
	
	#print "$parentchr\t$pairedchr\t$childcontig\t$childorientation\n";
	$ordinal = "0" if !exists $map{$pairedchr};
	my $prev_contig = "NONE";
	$prev_contig = $map{$pairedchr}{$ordinal-1}{childcontig} if $ordinal > 0;
	
	if ($prev_contig ne $childcontig){
		if(length($childcontig) > 0){
			$map{$pairedchr}{$ordinal}{parentchr}=$parentchr;
	        $map{$pairedchr}{$ordinal}{childcontig}=$childcontig;
	        $map{$pairedchr}{$ordinal}{childorientation}=$childorientation;
		
		    ++$ordinal;
		}
	}
    
	
}

print Dumper(\%map) if $options{debug};


foreach my $chr (sort keys %map){
	my $susucontig ="" ;
	my $track = "";
	foreach my $ord (sort {$a<=>$b} keys %{$map{$chr}}){
	   print "$chr\t$ord\t$map{$chr}{$ord}{childcontig}\t$map{$chr}{$ord}{childorientation}\n" if $options{debug};
	   	my $contig = $$contigs_href{$map{$chr}{$ord}{childcontig}};
	   	$contig = &reverse_compliment($contig) if $map{$chr}{$ord}{childorientation} eq "R";
	   	$susucontig = $contig if $ord == 0;
	   	$susucontig .= $contig if $ord > 0; 
	   	$susucontig .= $insert;

	   	$track .= $map{$chr}{$ord}{childcontig}.$map{$chr}{$ord}{childorientation}.'_';

	}
	$track =~ s/_$//;
	$susucontig =~ s/$insert$//;
	print ">susu_$chr \| $track\n";
	print $susucontig."\n\n";
	
}


sub fasta2hash(){
    my $fastafile = shift;
    my %fastahash = ();
    my $seq_in = Bio::SeqIO->new(-file   => $fastafile,
                                 -format => "fasta" );
    while (my $inseq = $seq_in->next_seq) {
        my $seqid = $inseq->id;
        my $seq = $inseq->seq();
        $seq = uc($seq);
        $fastahash{$seqid}=$seq;   
    }
    return (\%fastahash);
}


sub reverse_compliment(){
	my ($seq) = @_;
	chomp($seq);
	$seq =~ tr/ATGCNatgcn/TACGNtacgn/;
	$seq = reverse($seq);
	return $seq;
}