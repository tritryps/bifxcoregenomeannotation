#!/depot/perl-5.12.1/bin/perl
#
#  Split features on chromosome onto contigs using AGP/GFF file
#
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'mapfile|m=s',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0
    -m, --mapfile
    	map file
    	
    --prefix
		something to name a file
		Default: prefix
	
		
    -h, --help
    -d, --debug
};

print $usage if $options{help};

#reading in map file
open(READ, $options{mapfile});
while(my $line = <READ>){
	chomp($line);
	my($newid, $oldid) = split("\t", $line);
	my $oldgff = $oldid;
	my $newgff = $newid.".old.gff";
	my $finalgff = $newid."final.gff";
	
	my $fparent;
	
	my $gffio = Bio::Tools::GFF->new( -file => $newgff,
									  -gff_version => 3);
	while(my $feature = $gffio->next_feature()){
		my $gffstr = $gffio->gff_string($feature);
		my $ftype = $feature->primary_tag;
		print $gffstr."\n" if $ftype eq "gene";
		print $gffstr."\n" if $ftype eq "mRNA";
		$fparent = $feature->seq_id
	}
	my $gffio2 = Bio::Tools::GFF->new( -file => $oldgff,
									  -gff_version => 3);
	while(my $feature = $gffio2->next_feature()){
		my $ftype = $feature->primary_tag;
		if($ftype ne "gene" && $ftype ne "mRNA"){
			$feature->seq_id($fparent);
			$feature->source_tag("SeattleBioMed");
			my $parent_tag = $newid."-mRNA-1";
			$feature->remove_tag('ID') if $feature->has_tag('ID');
	    	$feature->add_tag_value('Parent', $parent_tag) if $ftype ne 'gene';
			
			my $gffstr = $gffio->gff_string($feature);
			print $gffstr."\n";
		}
	}
	
		print "\n\n";
									  

	
	
}
