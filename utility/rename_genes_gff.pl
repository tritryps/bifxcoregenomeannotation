#!/depot/perl-5.12.1/bin/perl
#
#  Rename ids in a gff from left to right of a contig
#  
#
##############
use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'feat_type|t=s',
              'prefix=s',
              'idprefix=s',
              'idsuffix=s',
              'addzeroatend',
	      'firstgene_count=s',
              'all_subfeat',
              'evi_file=s',
              'evi_tags=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    --gff, -g
        GFF file which contains the genes to be renamed.
    
    --feat_type, -t
        Type of the feature[gene|mRNA|CDS]
        Default: gene
    --idprefix
    	Prefix this word to the new geneid[eg., FAILED]
    	FAILED_contigid.013    
    --idsuffix
    	Suffix this word to the new geneid[eg., FAILED]
    --addzeroatend
        if this is set, ids are appended with a '0' [zero] 
        so that there is a 9 id gap between each gene
    --firstgene_count
	Default:1
	By default, the gene counter (to name gene) starts at one. So this will give rise to idprefix.0010. But this
	can be reset to start in any number. --first_genecount=481 will set the first id to idprefix.4810.
    --all_subfeat
    	Set this if you want to print all the subfeatures as well.
    	Note, with this option, these subfeatures does not get new id, or new parent set automatically
    	Run output of this script thru another script to achieve that. 	
    --prefix
        something to prefix ids a file
        Default: none 
	--evi_file
		evidence file in tab delimited by tab
		first column=gff id (old)
		rest of columns = "0" -> No evidence ">=1" -> evidence available
		must also set --evi_tags, which is header for evidence score columns (2 thru infinity)
	--evi_tags
		evidence tags/column headers for 2 trh infinity in evi_file
    
    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
$options{idsuffix} ||='0';

## read in gff
warn "reading GFF\n";
my ($gff_href, $gffByStart_href) = &hasify_multicontig_gff($options{gff}, $options{feat_type});
my %gff = %$gff_href;
my %gffByStart = %$gffByStart_href;

# RENAME
my %renamed = ();

foreach my $contig(sort keys %gffByStart){
	print "Renaming $contig\n" if $options{debug};
	my $genecounter = "1";
	$genecounter = $options{firstgene_count} if $options{firstgene_count};
	foreach my $startpos (sort {$a <=> $b} keys %{$gffByStart{$contig}}){
		my $id  = sprintf("%03d", $genecounter);
		my $geneid ;
        $geneid = $contig.'.'.$id;
		$geneid = $contig.'.'.$id."0" if $options{addzeroatend};
		#$geneid = $contig.'.snoRNA'.$id."0" if $options{addzeroatend};
        #$geneid = $contig.'.tRNA.'.$id."0" if $options{addzeroatend};
        #$geneid = $contig.'.rRNA.'.$id."0" if $options{addzeroatend};
        #$geneid = $contig.'.snRNA.'.$id."0" if $options{addzeroatend};
        #$geneid = $contig.'.SRP.RNA.'.$id."0" if $options{addzeroatend};
        $geneid = $options{idprefix}.".".$geneid if $options{idprefix};
		#$geneid .= $options{idsuffix} if $options{idsuffix};
		#print "$contig\t$startpos\t$geneid\t$gffByStart{$contig}{$startpos}{fID}\n";
		++$genecounter;
		$renamed{$gffByStart{$contig}{$startpos}{fID}}=$geneid;
	}
}
print Dumper(\%renamed) if $options{debug};

# read in evidence tab file
my %evi = ();
if($options{evi_file} && $options{evi_tags}){
	my @evitags = split(',', $options{evi_tags});
	
	open(READ, $options{evi_file});
	while(my $line = <READ>){
		my ($id, @eviscores) = split("\t", $line);
		
		my $step = "0";
		foreach my $tag (@evitags){
			$evi{$id}{$evitags[$step]}=$eviscores[$step];
			++$step;
		}
	}
}
print Dumper(\%evi) if $options{debug};

my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                          -gff_version => 3);
                                      
        my $geneId = "";
        while(my $feature = $gffio->next_feature()){
        
            my $ftype = $feature->primary_tag;
            my $contigid = $feature->seq_id;
            if($ftype eq $options{feat_type}){
            	my ($currentid) = $feature->each_tag_value("ID");
            	print "$currentid\n" if $options{debug};
            	my $newid = $renamed{$currentid};
            	#print "$contigid\t$currentid\t$newid\n";
				
				
				$feature->remove_tag('ID') if $feature->has_tag('ID');
            	$feature->add_tag_value('ID', $newid);
            	$feature->remove_tag('Name') if $feature->has_tag('Name');
            	$feature->add_tag_value('Name', $newid);
            	#$feature->remove_tag('Alias') if $feature->has_tag('Alias');
            	$feature->add_tag_value('Alias', $currentid);
            	$feature->remove_tag('note') if $feature->has_tag('note');
            	$feature->remove_tag('gscan') if $feature->has_tag('gscan');
            	$feature->remove_tag('tcode') if $feature->has_tag('tcode');
            	$feature->remove_tag('codon') if $feature->has_tag('codon');
            	if($options{evi_file}){
            		if(exists $evi{$currentid}){
            			foreach my $tag (sort keys %{$evi{$currentid}}){
            				print $tag."\n" if $options{debug};
            				my $torf = 'FALSE';
            				$torf = 'TRUE' if $evi{$currentid} > "0";
            				$feature->remove_tag($tag) if $feature->has_tag($tag);
            				$feature->add_tag_value($tag, $torf);
            				
            			}
            		}
            	}
            	
            	my $gffstring = $gffio->gff_string($feature);
            	print $gffstring."\n";
            }
            else{
            	my $gffstring = $gffio->gff_string($feature);
            	print $gffstring."\n" if $options{all_subfeat};
            	
            }
        }
        

sub hasify_multicontig_gff(){
    my ($gff_files, $type) = shift;
    my %gff = ();
    my $gffByStart =();                             

    my @files = split(',', $gff_files);
    foreach my $file (@files){
        if(-e $file){
        my $gffio = Bio::Tools::GFF->new( -file => $file,
                                          -gff_version => 3);
                                      
        my $geneId = "";
        while(my $feature = $gffio->next_feature()){
        
            my $ftype = $feature->primary_tag;
            my $contigid = $feature->seq_id;
            if($ftype eq $options{feat_type}){
            	    my $attr_href = &process_attrs2($feature);
            	    my %attr = %$attr_href;    
                    my @fid = $feature->each_tag_value("ID");
                    my $geneid = $fid[0];
                    $gff{$contigid}{$geneid}{"fparent"}=$feature->seq_id;
                    $gff{$contigid}{$geneid}{"fmin"}=$feature->start;
                    $gff{$contigid}{$geneid}{"fmax"}=$feature->end;
                    $gff{$contigid}{$geneid}{"fstrand"}=$feature->strand;
                    $gff{$contigid}{$geneid}{"ftype"}=$feature->primary_tag;
                    $gff{$contigid}{$geneid}{"fframe"}=$feature->frame;
                    $gff{$contigid}{$geneid}{"fsource"}=$feature->source_tag;
                    $gff{$contigid}{$geneid}{"fscore"}=$feature->score;
                    $gff{$contigid}{$geneid}{"fattrs"}=$attr_href;
                    
                    $gffByStart{$contigid}{$feature->start}{fID} = $geneid;
                }
            
            }
        }
    } 
    print Dumper(\%gff)  if $options{debug};
    print Dumper(\%gffByStart)  if $options{debug};
    return (\%gff, \%gffByStart);
}


sub process_attrs2{
    my $feat = shift;
    
    my @tags = $feat->get_all_tags();
    
    my $attr;
    my %attr;
    foreach (@tags){
        my $tag = $_;
        my @values = $feat->get_tag_values($tag);
        foreach (@values){
            my $value = $_;
            my $quote_it;
            
            $quote_it++ if $value =~ /\s+/;
            $quote_it++ if $value =~ /;/;
            
            $value = '"'. $value . '"' if $quote_it;
            $attr{$tag} = $value;
        }
        
    }
    
  return \%attr;
}
