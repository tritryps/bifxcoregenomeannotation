#!/depot/perl-5.12.1/bin/perl
#
#
# PURPOSE: splits a blast output file into multiple blast output files
#          One file per query. Also encloses the contents with  <PRE> </PRE> tags
#
# USAGE:  $0 [options] -ref_fasta=<Fasta with annotation> -rank=<rank of splic>target_fasta=<target fasta> -blast_file=<blast file with -m8 option>
#
# Author: gowthaman.ramasamy@seattlebiomed.org
################################################################################################################
use strict;
use warnings;
use Bio::SeqIO;
use DateTime;
use Data::Dumper;
use Bio::Tools::GFF;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my $usage =qq{
$0 [options] -ref_fasta=<source fasta file name> -rank=<rank of splic> -target_fasta=<target fasta> -blast_file=<blast file with -m8 option>


OPTIONS:
     --source_fasta
        Name of source fasta file containing query sequences
    --blast_files
        blastp/blastn output file in the align format
        In case of multiple files give comma separated list
    --prefix
   		prefix for the output file
    --db_name
   		name of the blast database. Used to create output files
   			    
    
    -help
        this help message
    -debug
};
my %options =();
my $opt_obj = GetOptions (\%options,
              'source_fasta=s',
              'blast_files=s',
              'db_name=s',
              'prefix=s',
              'debug|d',
              'help|h',
              );

die("$usage\n") if($options{help});
$options{db_name} ||= "blastdb";
#----------------------------------------------------------------------
# Handle command line
#----------------------------------------------------------------------
foreach my $file (split(",", $options{blast_files})){
	open(READ, $file);
	my $content ="";
	my ($query_id, $outfile);

	while(my $line = <READ>){
		
		if($line =~ /^BLAST/){
			#end of previous hit. write it out
			$query_id =~ s/\s//g;
			$outfile = $query_id.'.vs.'.$options{db_name}.".blastout.align";
			$content .= '</PRE>';
			open(WRITE, ">>$outfile");
			print WRITE $content;
			warn "$outfile \n#############################\n\n";
			$content = '<PRE>'."\n";
			
		}else{
			$query_id = $line if $line =~ /^Query=/;
			$query_id =~ s/Query= // if $line =~ /^Query=/;
			$content .= $line;
		}
		
	}
}
