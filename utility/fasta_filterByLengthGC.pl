#!/depot/perl-5.12.1/bin/perl
#
# Filter set of fasta séquences for length.
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'fasta|f=s',
		      'minlen=s',
		      'maxlen=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    -f, --fasta
        multifasta with all reads
    --minlen
		Minimum length to keep
		Default: 1
	--maxlen
		Maximim length of the fasta record to keep
		Default:100,000,000

    -h, --help
    -d, --debug
};

$options{source} ||= "gff";

$options{minlen} ||= 1;
$options{maxlen} ||= 100000000;


#read fasta
#read fastq
my $seq_in = Bio::SeqIO->new(-file   => $options{fasta},
                             -format => "fasta" );
my %seqs = ();
while (my $inseq = $seq_in->next_seq) {
        my $seqid = $inseq->id;
        my $seq = $inseq->seq();
		my $len=length($seq);
		if($len >= $options{minlen} && $len < $options{maxlen}){
			print ">$seqid\n$seq\n";	
		}
}