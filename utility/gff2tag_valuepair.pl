#!/depot/perl-5.12.1/bin/perl
#
#  Pull a tag value pair for each id
#   eg: geneid-product name
#   eg: geneid-product source
#
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Data::Dumper;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'feat_type|f=s',
              'tag|t=s',
              'example',
              'debug|d',
              'help|h');

my $usage = qq{
$0
    -g, --gff
        gff
    -f, --feat_type
    	Feature type
    -t, --tag
    	Tag name
        
    -h, --help
    -d, --debug
    --example
	
};

my $example = qq{
$0 -gff CfaC1.v13.3.5_pseudochr.proteincoding.genes.gff3_v2 -feat_type gene -tag product 
};
die($usage) if $options{help};
die($example) if $options{example};

my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
									  -gff_version => 3);

while(my $feature = $gffio->next_feature()){
	my $ftype = $feature->primary_tag;
	if ($ftype eq $options{feat_type}){
		my($id) = $feature->each_tag_value("ID");
		my $value = "NONE AVAILABLE";
		($value) = $feature->each_tag_value($options{tag}) if $feature->has_tag($options{tag});
		$value ||= 'NA';
		#$value .= '"' if $value !~ /\"$/;
		print "$id\t$value\n";
	}
}

