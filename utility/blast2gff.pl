#!/depot/perl-5.12.1/bin/perl
#
#  blast to gff
#
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Data::Dumper;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'blastfile|b=s',
              'transfer_all_hits',
              'evalue=s',
              'bitscore=s',
              'coverage=s',
              'identity=s',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0
    -b, --blastfile
        tab file
    -transfer_all_hits
    	when set, all the hsps for each query will be have a gff feature. 
    	The primary feature will have the original query name.
    	OTHers will have .1+ attached to it.
    -evalue
    	max evalue
    -bitscore
    	min bitscore
    -identity
    	min identity
    -coverage
    	min coverage
    
    	
        
    --prefix
		something to name a file
		Default: prefix
	
		
    -h, --help
    -d, --debug
};

print $usage if $options{help};
$options{evalue} ||= 0.001;
$options{bitscore} ||= 50;
$options{identity} ||= 50;


my $blast_href = hasifyblast($options{blastfile});
sub hasifyblast(){
	my $file = shift;
	my %data = ();
	open(READ, $file);
	my $previous_hit; my $previous_query;
	my $secondary_hit_count = "0";
	while(my $line =<READ>){
		chomp($line);
		my($query, $hit, $idty, $alignlen, $mm, $gaps, $qstart, $qend, $sstart, $send, $evalue, $bitscore)=split("\t", $line);
		if($bitscore >= $options{bitscore} && $evalue <= $options{evalue} && $idty >= $options{identity}){		
			my $fid = $query;
			my $isprimary='YES';
			if($query eq $previous_query){
				++$secondary_hit_count;
				$fid = $query.'.'.$secondary_hit_count;
				$isprimary='NO';
			}else{
				$secondary_hit_count = "0";
			}
			
			my $strand;
			$strand = '+' if $sstart < $send;
			$strand = '-' if $send < $sstart;
			
			print "$hit\tTschudiLab\tgene\t$sstart\t$send\t.\t$strand\t.\tID=$fid\;primary_location=$isprimary\;identity=$idty\;bitscore=$bitscore\;\n" if $strand eq '+';
			print "$hit\tTschudiLab\tgene\t$send\t$sstart\t.\t$strand\t.\tID=$fid\;primary_location=$isprimary\;identity=$idty\;bitscore=$bitscore\;\n" if $strand eq '-';

			$previous_query=$query;
		}
	}
}


#=cut

=head
#table s7	
	my($chr, $id, $st, $end, $len, $strand, $abundance) = split("\t", $line);
	if($strand eq '+'){
		print "$chr\tTschudiLab\tgene\t$st\t$end\t.\t$strand\t.\tID=$id;rna_abundance=\"$abundance\"\n";
	}else{
		print "$chr\tTschudiLab\tgene\t$end\t$st\t.\t$strand\t.\tID=$id;rna_abundance=\"$abundance\"\n";
	}
=cut
	
