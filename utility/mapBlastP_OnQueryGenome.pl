#!/depot/perl-5.12.1/bin/perl
#
#  map blastp restuls on the query genome.
#
#
##############
use strict;
use warnings;
use Bio::SeqIO;
use Bio::SearchIO;
use Bio::Tools::GFF;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;
use Array::IntSpan;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'feat_type|t=s',
              'blastoutfile=s',
              'productnamesfile=s',
              'sourcetag=s',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0
    --gff, -g
        GFF file which contains the genes to be renamed.
    --feat_type, -t
        Type of the feature[gene|mRNA|CDS]
        Default: gene
    --blastoutfile
        align output file from blast
    --productnamesfile
        tab delimited file with id and product names
    --sourcetag
    	source tag for the gff(second column)
    --prefix
        something to prefix ids a file
        Default: none

    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
$options{sourcetag} ||= "blast";

# Read in gff
my %gff = ();
my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                          -gff_version => 3);
my $geneId = "";
while(my $feature = $gffio->next_feature()){
    my $ftype = $feature->primary_tag;
    my $contigID = $feature->seq_id;
    if($ftype eq 'CDS'){
        my ($cdsID) = $feature->each_tag_value("ID") if $feature->has_tag('ID');
        my ($mRNAID, $cdsOrdinal) = $cdsID =~ m/(.+):CDS:(\d)/g;
        #print "$contigID, $cdsID, $mRNAID, $cdsOrdinal\n";
        $gff{$mRNAID}{$cdsOrdinal}{'fmin'}   = $feature->start;
        $gff{$mRNAID}{$cdsOrdinal}{'fmax'}   = $feature->end;
        $gff{$mRNAID}{$cdsOrdinal}{'strand'} = $feature->strand;
    	$gff{$mRNAID}{'supercontig'} = $contigID;
    }

}
#print Dumper(\%gff);

# read in product names
my %productNames = ();
open(READ, $options{productnamesfile});
while (my $line=<READ>){
	chomp($line);
	my($id, $des) = split("\t", $line);
	$productNames{$id}=$des;
}

# Read in blastoutput
my $in = new Bio::SearchIO( -format => 'blast',
                            -file   =>  $options{blastoutfile});

while ( my $result = $in->next_result ){
    my ($queryName, $queryLen) = ($result->query_name, $result->query_length);
    print $queryName."\n" if $options{debug};
    my $CfgeneDetails = $gff{$queryName};
    #print Dumper($CfgeneDetails);

    # for each hit
    my $isFirstHit = "YES";my $hitCount = "0";
    while(my $hit = $result->next_hit){
        if ($isFirstHit eq 'YES'){
            my $hitName = $hit->name;
            my $noofHsps = $hit->num_hsps;
            print "\t$hitName\t$noofHsps\n" if $options{debug};
            my @hsps = $hit->hsps;

            # for each hsp
            while(my $hsp = $hit->next_hsp){
                print "\nhsp is here. But strand is $$CfgeneDetails{'1'}{'strand'}\n" if $options{debug};
                #my $hsp             = $hsps[$hspOrdinal];
                my $evalue          = $hsp->evalue;
                my $fracIdentical   = $hsp->frac_identical;
                my $fracConserved   = $hsp->frac_conserved;
                my $percIdentical   = sprintf( "%d", $hsp->percent_identity);
				my $covr_query = sprintf( "%d", 100 * $hsp->length('query') / $result->query_length );
				my $covr_hit = sprintf( "%d", 100 * $hsp->length('hit') / $hit->length );
				
                my $length_hsp      = $hsp->length('total');
                my $length_hit      = $hsp->length('hit');
                my $length_query    = $hsp->length('query');
                my $bitScore        = $hsp->bits;
                my ($qStart, $qEnd) = $hsp->range('query');
                my ($hStart, $hEnd) = $hsp->range('hit');
				
				my $tagValues = "Name=$hitName;";
				$tagValues .= "hit_description=\"$productNames{$hitName}\";";
				$tagValues .= "evalue=$evalue;bitscore=$bitScore;identity=$percIdentical%;";
				$tagValues .= "cov_of_query=$covr_query%;";
				$tagValues .= "cov_of_hit=$covr_hit%;";
				$tagValues .= "CfaQuery=$queryName;";
				
				# filter low quality hits
				my $feat_type = 'blast_hsp';
				if($evalue > 1e-3 || $bitScore < 100 || $covr_query < 50){
					$feat_type = 'blast_hsp_lowquality';
				} 
				
				
                if($$CfgeneDetails{'1'}{'strand'} == 1){
                    my $matchStart = $$CfgeneDetails{'1'}{'fmin'} + ($qStart *3);
                    my $matchEnd   = $$CfgeneDetails{'1'}{'fmin'} + ($qEnd *3);

                    print "$$CfgeneDetails{'supercontig'}\t$options{sourcetag}\t$feat_type\t$matchStart\t$matchEnd\t.\t+\t.\t$tagValues\n";
                }elsif($$CfgeneDetails{'1'}{'strand'} == -1){
                    my $matchStart = $$CfgeneDetails{'1'}{'fmax'} - ($qStart *3);
                    my $matchEnd   = $$CfgeneDetails{'1'}{'fmax'} - ($qEnd *3);

                    print "$$CfgeneDetails{'supercontig'}\t$options{sourcetag}\t$feat_type\t$matchStart\t$matchEnd\t.\t-\t.\t$tagValues\n";
                }
            }
            $isFirstHit ='NO';
        }
    }
}