#!/depot/perl-5.12.1/bin/perl
#
# Purpose: To compare SBRI and TriTryp GFFs to obtain different subsets for
#          annotating GENEDB
#
# Input : GFF3
#
################################

use strict;
use warnings;
use Data::Dumper;
use Bio::SeqIO;
use Bio::DB::GFF;
use Bio::DB::Fasta;
use Array::IntSpan;
use Bio::Tools::GFF;
use Math::Round;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'sbrigff=s',
              'gff=s',
              'fasta=s',
              'feat_type=s',
			  'find_modified_atg',
			  'ignore_ids=s',
              'debug',
              'help|h'
              );

my $usage = qq{
$0 
	--gff
		GFF file
	--sbrigff
		GFF file from sbri
	--fasta
		Fasta of the genome contigs	
	--feat_type
		Feature type
	-r, --rawcount
		Row count file with 5 columns
		Output of 
		<CHR><Position><TotalReads><PosStrandReads><NegStrandReads><MajorStrand>
	-bpgff
		breakpoint gff
	-utrdata 
		UTR status file in tab format <Gene><5utr><3utr>	
	--count_type
		maximal|slsite
	--ignore_ids
		Ignore these features when reading GFF or constructing coordinates
	
};

print "$usage\n" if($options{help});
$options{minPercentDiff} ||= 10;
$options{count_type} = 'maximal';
#----------------------------------------------------------------------
# Main - Read input
#----------------------------------------------------------------------

# Read ignore ids list
my %ignore = ();
if($options{ignore_ids}){
	open(READ, $options{ignore_ids});
	while(my $line = <READ>){
		chomp($line);
		$ignore{$line}=1;
	}
}


