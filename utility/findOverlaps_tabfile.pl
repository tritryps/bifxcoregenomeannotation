#!/depot/perl-5.12.1/bin/perl
#
#
# PURPOSE: Find overlapping coordinate
#
#
# Author: gowthaman.ramasamy@seattlebiomed.org
################################################################################################################
use strict;
use warnings;
use Bio::SeqIO;
use DateTime;
use Data::Dumper;
use Bio::Tools::GFF;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my $usage =qq{
$0 [options] -ref_fasta=<source fasta file name> -rank=<rank of splic> -target_fasta=<target fasta> -blast_file=<blast file with -m8 option>


OPTIONS:
    --tabfile
        Name of source fasta file containing query sequences
    
        
    -help
        this help message
    -debug
};
my %options =();
my $opt_obj = GetOptions (\%options,
              'tabfile=s',
              
              'debug|d',
              'help|h',
              );

die("$usage\n") if($options{help});

my %data = ();
open(READ, "$options{tabfile}");
my $neworder=0;
while (my $line=<READ>){
	chomp($line)
	++$neworder;
	my($scaf, $chr, $order, $dir, $start, $end) = split("\t", $line);
	my $source = "SBRI";
	my $feat_type= "contig";
	print "Chr$chr\t$source\t$feat_type\t$start\t$end\t.\t$dir\t.\tID=$scaf\n";
	
}

