#!/depot/perl-5.12.1/bin/perl
#
# Purpose: Create a spread sheet from set of blast out files
#
# Input : list of query gene ids, variables in the file names
# ./spreadsheet_forblastouts.pl -i CfaC1.all.protein.ids -variable_filenames LmjF,LbrM,LinJ,LdoB,LmxM,LtaP,Tryps > CfaC1.annotationV.6_browse_blastoutfiles.xls
################################

use strict;
use warnings;
use Data::Dumper;
use Bio::SeqIO;
use Bio::DB::GFF;
use Bio::DB::Fasta;
use Array::IntSpan;
use Bio::Tools::GFF;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);
#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'idfile|i=s',
              'variable_filenames=s',
              'debug',
              'help|h');

my $usage = qq{
$0 
	-i, --idfile
		GFF file
	--variable_filenames
		Variable file names, comma separated
		eg, LbrM,LmjF,Tryps,LmxM
	example:
	./spreadsheet_forblastouts.pl -i CfaC1.all.protein.ids -variable_filenames LmjF,LbrM,LinJ,LdoB,LmxM,LtaP,Tryps > CfaC1.annotationV.6_browse_blastoutfiles.xls
	
};

print "$usage\n" if($options{help});


#----------------------------------------------------------------------
# Main - Read input
#----------------------------------------------------------------------
open(READ, "$options{idfile}");
while(my $line = <READ>){
	chomp($line);
	my $id = $line;
	print "$id";
	foreach my $var (split(',', $options{variable_filenames})){
		my $path2file = "blastout/vs$var/$id.vs.$var.blastout.html";
		my $cell_value = $id.' vs '.$var;
		my $link_text = '=Hyperlink("'.$path2file.'","'.$cell_value.'")';
		
		print "\t".$link_text;
	}
	print "\n";
}

#----------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------
