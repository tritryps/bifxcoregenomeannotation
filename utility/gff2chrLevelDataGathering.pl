#!/depot/perl-5.12.1/bin/perl
#
#  blast to gff
#
#
#
##############


use strict;
use warnings;
use POSIX;
use Data::Dumper;
use Math::Round;
use Bio::Perl;
use Bio::SeqIO;
use Bio::DB::GFF;
use Bio::Tools::GFF;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'feat_type|f=s',
              'descriptions_inorder',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0
    -g, --gff
        gff file
    -f, --feat_type
    	feat type
    	Default : gene
    -descriptions_inorder
    	Set this, if you want to print the description in sorted way. 
    	This helps to find what kind of genes are there in each chr
    	this however, wont print the gene ids    
    --prefix
		something to name a file
		Default: prefix
	
		
    -h, --help
    -d, --debug
};

print $usage if $options{help};
$options{feat_type} ||= 'gene';

# Read in gff file again to iterate over CDS
my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                      -gff_version => 3);

my %gff = ();
my %gff_bystart = ();

#walk thru a feature at a time (ignore everything other than an exon).
my $count = "0";

while(my $feature = $gffio->next_feature()){
    my $type = $feature->primary_tag;
    if($type eq $options{feat_type}){
    	my $contig = $feature->seq_id;
    	
    	++ $gff{$contig}{genecount};
		
    	my ($description) = $feature->each_tag_value("description");
    	$gff{$contig}{descriptions} .= "##".$description;
    	++$gff{$contig}{pseudogene} if $description =~ /pseudogene/;
    	++$gff{$contig}{unlikely} if $description =~ /unlikely/;
    	++$gff{$contig}{ESAG} if $description =~ /ESAG/;
    	
    	my ($fid) = $feature->each_tag_value("ID");
        my $fmin = $feature->start;
        my $fmax = $feature->end;
        my $strand = $feature->strand;
    	my $phase = $feature->frame;
    	my $source = $feature->source_tag;
        my $score = $feature->score;
        my $ftype = $feature->primary_tag;
        
        $gff_bystart{$contig}{$fmin}{fmax}= $fmax ;
        $gff_bystart{$contig}{$fmin}{strand}= $strand ;
        $gff_bystart{$contig}{$fmin}{ftype}= $ftype ;
        $gff_bystart{$contig}{$fmin}{fid}= $fid ;
        $gff_bystart{$contig}{$fmin}{des}= $description ;

	
    }
}

print Dumper(\%gff) if $options{debug};

foreach my $contig (keys %gff){
	next if $contig =~ /^Tb927_/;
	my @des = split("##", $gff{$contig}{descriptions});
	my @des_sorted = sort(@des);
	
    $gff{$contig}{unlikely} = "0" if !$gff{$contig}{unlikely};
	$gff{$contig}{pseudogene} = "0" if !$gff{$contig}{pseudogene};
	$gff{$contig}{ESAG} = "0" if !$gff{$contig}{ESAG};

	print "\n\n#ContigSummary\t$contig\t$gff{$contig}{genecount}\t$gff{$contig}{unlikely}\t$gff{$contig}{ESAG}\t$gff{$contig}{pseudogene}\n";
	if($options{descriptions_inorder}){
		foreach my $des(@des_sorted){
			print "\t$des\n";
		}
	}else{
		foreach my $fmin (sort {$a<=>$b} keys %{$gff_bystart{$contig}}){
 			print "$gff_bystart{$contig}{$fmin}{fid}";
			print "\t$contig\t$fmin";
			print "\t$gff_bystart{$contig}{$fmin}{fid}";
			print "\t$gff_bystart{$contig}{$fmin}{strand}";
			print "\t$gff_bystart{$contig}{$fmin}{des}";
			
			print "\n";
			
		}
	
	
	}
	
}

