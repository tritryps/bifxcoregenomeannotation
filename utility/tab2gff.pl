#!/depot/perl-5.12.1/bin/perl
#
#  xls to gff
#
#
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Data::Dumper;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'tab|t=s',
              'prefix=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0
    -t, --tab
        tab file
        
    --prefix
		something to name a file
		Default: prefix
	
		
    -h, --help
    -d, --debug
};


open(READ, $options{tab});
while(my $line = <READ>){
	chomp($line);


     	my($id, $source, $chr, $strand, $st, $end, $trypst, $trypend, $something, $isPseudo, $description, $oldname) = split("\t", $line);
        if($strand eq '+'){
                print "$chr\t$source\tCDS\t$st\t$end\t.\t$strand\t.\tID=$id;product=$description\n";
        }else{
		print "$chr\t$source\tCDS\t$end\t$st\t.\t$strand\t.\tID=$id;product=$description\n";

        }

=head
	my($chr, $id, $st, $end, $len, $strand, $abundance, $msms, $orf, $peptidefeatures, $putativehomologues) = split("\t", $line);
	if($strand eq '+'){
		print "$chr\tTschudiLab\tgene\t$st\t$end\t.\t$strand\t.\tID=$id;rna_abundance=\"$abundance\";msms_evidence=\"$msms\";orf=\"$orf\";peptide_features=\"$peptidefeatures\";putative_homologues=$putativehomologues\n";
	}else{
		print "$chr\tTschudiLab\tgene\t$end\t$st\t.\t$strand\t.\tID=$id;rna_abundance=\"$abundance\";msms_evidence=\"$msms\";orf=\"$orf\";peptide_features=\"$peptidefeatures\";putative_homologues=$putativehomologues\n";
	}
=cut

=head
#table s7	
	my($chr, $id, $st, $end, $len, $strand, $abundance) = split("\t", $line);
	if($strand eq '+'){
		print "$chr\tTschudiLab\tgene\t$st\t$end\t.\t$strand\t.\tID=$id;rna_abundance=\"$abundance\"\n";
	}else{
		print "$chr\tTschudiLab\tgene\t$end\t$st\t.\t$strand\t.\tID=$id;rna_abundance=\"$abundance\"\n";
	}
=cut

# tabformat : id, chr, strand, start, end (not fmin/fmax)
=head
	my($id, $chr, $strand, $st, $end) = split("\t", $line);
	if($strand eq '+'){
		print "$chr\tSBRI\tCDS\t$st\t$end\t.\t$strand\t.\tID=$id;\n";
	}else{
		print "$chr\tSBRI\tCDS\t$end\t$st\t.\t$strand\t.\tID=$id;\n";
	}
=cut

# tabformat : id, source, chr, strand, start, end (not fmin/fmax)
=head
      	my($id, $source,  $chr, $strand, $st, $end) = split("\t", $line);
        if($strand eq '+'){
                print "$chr\t$source\tCDS\t$st\t$end\t.\t$strand\t.\tID=$id;color=219 146 11\n";
        }else{
              	print "$chr\t$source\tCDS\t$end\t$st\t.\t$strand\t.\tID=$id;color=219 146 11\n";
        }
=cut

# tabformat : chr, fmin fmax, $score
=head
        my($chr, $fmin, $fmax, $score) = split("\t", $line);
        print "$chr\tJ-IP\tregion\t$fmin\t$fmax\t$score\t+\t.\tID=$score\n";
=cut
	

# tabformat : id, chr, strand, start, end (not fmin/fmax), pseudogene
=head
     	my($id, $chr, $strand, $st, $end, $pseudo) = split("\t", $line);
        if($strand eq '+'){
                print "$chr\tSBRI\tCDS\t$st\t$end\t.\t$strand\t.\tID=$id;\n";
        }else{
              	print "$chr\tSBRI\tCDS\t$end\t$st\t.\t$strand\t.\tID=$id;\n";
        }
=cut
}
