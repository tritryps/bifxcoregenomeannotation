#!/depot/perl-5.12.1/bin/perl
#
#  Count number of sub features (mRNA, CDS, exon) for each gene
#
##############


use strict;
use warnings;
use Bio::SeqIO;
use Bio::Tools::GFF;
use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;


use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'gff|g=s',
              'feat_type|f=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    -g, --gff
        gff file idealy output of chromosome2supercontig.pl script (via agp file)
    -f, --feat_type
        Type of the future to match and convert from
    
    -h, --help
    -d, --debug
};


die("$usage\n") if($options{help});
die("$usage\n") if(!$options{gff});

my $gffio = Bio::Tools::GFF->new( -file => $options{gff},
                                 -gff_version => 3);


my %data = ();

while(my $feature = $gffio->next_feature()){
    my ($fid)= $feature->get_tag_values("ID") if $feature->has_tag('ID') ;
    my ($parent)= $feature->get_tag_values("Parent") if $feature->has_tag('Parent');
    my $ftype = $feature->primary_tag;
    my $contig = $feature->seq_id;
    
    
    $parent =~ s/-mRNA-1// if $ftype eq "mRNA";
    $parent =~ s/-mRNA-1// if $ftype eq "exon";
    $parent =~ s/-mRNA-1//  if $ftype eq "CDS";
    
    $fid = $parent if ($ftype ne "gene");
    ++$data{$fid}{$ftype};
    
}                                

print Dumper(\%data);

foreach my $gene (keys %data){
	foreach my $ftype (keys %{$data{$gene}}){
		print "$gene\t$ftype\t$data{$gene}{$ftype}\n" if $data{$gene}{$ftype} > 1;
	}
}                             
