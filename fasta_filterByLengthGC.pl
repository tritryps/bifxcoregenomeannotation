#!/depot/perl-5.12.1/bin/perl
#
# finds a motif in a fasta and prints the entire fasta entry
#
##############


use strict;
use warnings;
use Bio::SeqIO;

use Bio::DB::Sam;
use Math::NumberCruncher;
use Data::Dumper;
use Math::Round;

use Getopt::Long qw(:config no_ignore_case no_auto_abbrev);


#----------------------------------------------------------------------
# handle command line
#----------------------------------------------------------------------
my %options =();
my $opt_obj = GetOptions (\%options,
              'fasta|f=s',
	      'motif|m=s',
              'debug|d',
              'help|h');

my $usage = qq{
$0 
    -f, --fasta
        multifasta with all reads
    -m, --motif
	motif

    -h, --help
    -d, --debug
};

$options{source} ||= "gff";


#read fasta
#read fastq
my $seq_in = Bio::SeqIO->new(-file   => $options{fasta},
                             -format => "fasta" );
my %seqs = ();
while (my $inseq = $seq_in->next_seq) {
        my $seqid = $inseq->id;
        my $seq = $inseq->seq();
	$seq =~ s/\*$//;
	my $len=length($seq);
	if($seq =~ /\*/){
		print ">$seqid\n$seq\n";
	}
}

